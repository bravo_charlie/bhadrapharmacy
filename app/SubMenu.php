<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubMenu extends Model
{
    protected $fillable = [
    	'order', 'subMenuName', 'mainmenuid'
    ];

    /**
	 * Products belonged to the multiple category
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function menu()
	{
	    return $this->belongsTo(Menu::class, 'mainmenuid');
	}

	 /**
     * Product Category associated with the Menu
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productCategory()
    {
        return $this->hasMany(ProductCategory::class, 'selectsubmenu')->orderBy('order');
    }
}
