<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //
    protected $fillable=[
        'user_id',
        'delivery_id',
        'delivery_price',
        'billing_email',
        'billing_name' ,
        'billing_address',
        'billing_city' ,
        'billing_province' ,
        'billing_postalcode',
        'billing_phone',

        'delivery_email',
        'delivery_name' ,
        'delivery_address',
        'delivery_city' ,
        'delivery_province' ,
        'delivery_postalcode',
        'delivery_phone',

        'billing_discount',
        'billing_discount_code' ,
        'billing_subtotal',
        'billing_tax',
        'billing_total',
        'error'
    ];

    /**
     * Ordered Products associated with the order
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orderProduct()
    {
        return $this->hasMany(OrderProduct::class);
    }

    /**
     * Delivery Id belonged to the order
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function delivery()
    {
        return $this->belongsTo(Delivery::class, 'delivery_id')->withTrashed();
    }
}
