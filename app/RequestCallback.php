<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestCallback extends Model
{
    protected $fillable = [
    	'name', 'telephone', 'email', 'time', 'comments'
    ];
}
