<?php

namespace App\Repositories;

use App\Product;
use App\SubMenu;

/**
 * 
 */
class HomePageRepository
{
	public function getSubmenuProducts($id, $request)
	{
		$sub_menu = SubMenu::with(['productCategory' => function($q) {
            $q->orderBy('order', 'ASC');
        }])->findorFail($id);

        $order = $request->sort;

        $sortUrl = '';

        $priceFilter = '';
        $count = !empty($request->count)?$request->count:12;
        if(isset($request->price)) {
            $priceFilter = $request->price;
            if($request->price === '5') {
                $products = Product::OfSubMenu($id)->where('price', '<', (int)$request->price);
            }
            else if($request->price === '50') {
                $products = Product::OfSubMenu($id)->where('price', '>', $request->price);
            } else {
                $priceRange = explode('-', $request->price);
                $products = Product::OfSubMenu($id)
                    ->where('price', '>', $priceRange[0])
                    ->where('price', '<=', $priceRange[1]);
            }
            $sortUrl = '?price='.$priceFilter.'&';
        } else {
            $products = Product::OfSubMenu($id);
            $sortUrl = '?';
        }

        if(!empty($order)) {
            $sortOrder = explode('_', $order);
            $products = $products->orderBy($sortOrder[0], $sortOrder[1]);
        }

        $products = $products->paginate($count);

        $sortArray = $this->getSortArray($sortUrl);

        $priceFilterArray = $this->getPriceFilterArray($order);

        return [
        	$sub_menu,$products,$priceFilter,$order,$sortArray, $priceFilterArray
        ];
	}

	public function getPriceFilterArray($order=null)
	{
		$orderUrl = (empty($order))?'':'&sort='.$order;
		return array(
			[
				'url' => '?price=5'.$orderUrl,
				'slug' => '5',
				'title' => 'Less than £5'
			],
			[
				'url' => '?price=5-10'.$orderUrl,
				'slug' => '5-10',
				'title' => '£5 to £10'
			],
			[
				'url' => '?price=10-15'.$orderUrl,
				'slug' => '10-15',
				'title' => '£10 to £15'
			],
			[
				'url' => '?price=15-20'.$orderUrl,
				'slug' => '15-20',
				'title' => '£15 to £20'
			],
			[
				'url' => '?price=20-30'.$orderUrl,
				'slug' => '20-30',
				'title' => '£20 to £30'
			],
			[
				'url' => '?price=30-40'.$orderUrl,
				'slug' => '30-40',
				'title' => '£30 to £40'
			],
			[
				'url' => '?price=50'.$orderUrl,
				'slug' => '50',
				'title' => 'Over £50'
			],
		);
	}

	public function getSortArray($sortUrl)
	{
		return array(
            [
                'url' => $sortUrl.'sort=price_asc',
                'slug' => 'price_asc',
                'title' => 'Price Low To High'
            ],
            [
                'url' => $sortUrl.'sort=price_desc',
                'slug' => 'price_desc',
                'title' => 'Price High To Low'
            ],
            [
                'url' => $sortUrl.'sort=name_asc',
                'slug' => 'name_asc',
                'title' => 'Title A-Z'
            ],
            [
                'url' => $sortUrl.'sort=name_desc',
                'slug' => 'name_desc',
                'title' => 'Title Z-A'
            ]
        );
	}

}