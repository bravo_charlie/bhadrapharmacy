<?php  

namespace App\Repositories;

use App\Product;
use App\ProductMultipleCategory;

use Exception;
/**
 * Checkout Repository class
 */
class ProductRepository
{
	/**
	 * Function to create the product.
	 */
	public function save($request) {
		  
		$images['featured_img1'] = $this->saveImage($request->file('featured_image1'));
		$images['featured_img2'] = $this->saveImage($request->file('featured_image2'));
		$images['featured_img3'] = $this->saveImage($request->file('featured_image3'));
		$dataToSave = $this->mapProductData($request, $images);
		$product = Product::create($dataToSave);

		$this->addMultipleCategories($product, $request->categories);

	}

	public function update ($request, $id) {
		
		$product = Product::findorFail($id);

		if($request->hasFile('featured_image1')){
		  $product->featured_image1 = $this->saveImage($request->file('featured_image1'));
		}
		if($request->hasFile('featured_image2')){
		  $product->featured_image2 = $this->saveImage($request->file('featured_image2'));
		}
		if($request->hasFile('featured_image3')){
		  $product->featured_image3 = $this->saveImage($request->file('featured_image3'));
		}

		$product->name = $request->name;
		$product->description = $request->input('description');
		$product->price = $request->input('price');
		$product->potency = $request->input('potency');
		$product->size = $request->input('size');
		$product->warnings = $request->input('warnings');
		$product->short_intro = $request->input('short_intro');
		$product->discount = $request->input('discount');
		$product->in_out_stock = $request->input('in_out_stock');
		$product->available_quantity = $request->input('available_quantity');
		$product->sku = $request->input('sku');
		$product->directions = $request->input('directions');
		$product->ingredients = $request->input('ingredients');
		// $product->advisory_information = $request->input('advisory_information');
		// $product->remember_to = $request->input('remember_to');
		$product->save();

		ProductMultipleCategory::where('product_id', $product->id)->delete();
		$this->addMultipleCategories($product, $request->categories);
	}

	/**
     * Add platforms to indicate where to
     * submit the post
     *
     * @param Product $product
     * @param array $categories
     */
    public function addMultipleCategories($product, $categories)
    {
    	foreach ($categories as $category) {
    		ProductMultipleCategory::create([
    						'product_id' => $product->id,
    						'product_category_id' => $category
    						]);
    	}
    }

	public function mapProductData($request, $images) {
		return [
				 'name'=>$request->name, 
				 'description'=>$request->description, 
				 'featured_image1'=>$images['featured_img1'], 
				 'featured_image2'=>$images['featured_img2'], 
				 'featured_image3'=>$images['featured_img3'], 
				 'price'=>$request->price, 
				 'potency'=>$request->potency, 
				 'size'=>$request->size,
				 'short_intro'=>$request->short_intro, 
				 'discount'=>$request->discount, 
				 'in_out_stock'=>$request->in_out_stock, 
				 'available_quantity'=>$request->available_quantity, 
				 'sku'=>$request->sku, 
				 'directions'=>$request->directions, 
				 'ingredients'=>$request->ingredients,
                 'warnings'=>$request->warnings


				];
	}

	public function saveImage($images) {
		$product_img_path="";
		if ($images) {
		    $destinationPathFeatured = 'images/productImages/';
		   	$product_img_path=time() . "_" . $images->getClientOriginalName();
		   	$images->move($destinationPathFeatured,$product_img_path);

			return $destinationPathFeatured.$product_img_path;

		}else{
		 	return 'images/productImages/defaultProdImg.jpg';
		}
	}
}