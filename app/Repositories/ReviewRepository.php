<?php

namespace App\Repositories;

use App\Review;
use Exception;

/**
 * Review Repository Class
 */

class ReviewRepository
{
	/**
	 * Create a new review
	 * @param type $request 
	 * @return type
	 */
	public function create($request) {
		$input = [
			'value_money'=> $request['value_money'],
            'quality'=> $request['quality'],
            'performance' => $request['performance'],
            'overall'=> $request['overall'],
            'name' => $request['name'],
            'email'=> $request['email'],
            'recommend' => $request['recommend'],
            'reviewBox' => $request['reviewBox'],
            'product_id'=> $request['product_id'],
            'user_id' => $request['user_id']
		];
		$result = Review::create($input);
		return $result;
	}
}