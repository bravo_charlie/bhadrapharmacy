<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ProductCategory extends Model
{
    protected $fillable=['name','description','featured_image','price','short_intro','discount','selectmainmenu','selectsubmenu','menu_id','sub_menu_id', 'order'];

    /**
     * Product Category can belongs to multiple categories
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productMultipleCategory()
    {
        return $this->hasMany(ProductMultipleCategory::class);
    }

    /**
     * Menu belonged to the product category
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function menu()
    {
        return $this->belongsTo(Menu::class, 'selectmainmenu');
    }

    /**
     * Sub Menu belonged to the product category
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subMenu()
    {
        return $this->belongsTo(SubMenu::class, 'selectsubmenu');
    }
}
