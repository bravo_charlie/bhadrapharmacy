<?php

namespace App\Http\ViewComposers;

use Illuminate\view\View;

use App\Repositories\MenuRepository;
/**
 * 
 */
class MenuComposer
{
	public $menu;
	
	public function __construct(MenuRepository $menuRepository)
	{
		$this->menu = $menuRepository->getMenus();
	}

	public function compose(View $view)
	{
		$view->with('menus', $this->menu);
	}
}