<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\ProductCategory;
use App\Product;
use App\Menu;
use App\SubMenu;

use App\Review;
use App\Repositories\ProductCategoryRepository;


class ProductCategoryController extends Controller
{
    protected $productCategoryRepository;

    public function __construct(ProductCategoryRepository $productCategoryRepository)
    {
        $this->productCategoryRepository = $productCategoryRepository;
    }

    public function index(){
      $mainmenu = Menu::pluck('menuName','id');

      //return $mainmenu;
     
        $submenu = SubMenu::pluck('subMenuName','id');
        //return $mainmenu;
        return view('admin.product.product_category_main', compact('mainmenu', 'submenu'));
    }

    public function store(Request $request){



        $product_cat_img = Input::file('featured_image');
        $product_cat_img_path="";
        if ($product_cat_img) {
            $destinationPathFeatured = 'images/categoryImages/';
            $product_cat_img_path=time() . "_" . $product_cat_img->getClientOriginalName();
            $product_cat_img->move($destinationPathFeatured,$product_cat_img_path);

            $featured_img=$destinationPathFeatured.$product_cat_img_path;

        }else{
            $featured_img='images/categoryImages/defaultCatImg.jpg';
        }

        ProductCategory::create([
            'name'=>$request->name,
            'order'=>$request->order,
            'description'=>$request->description,
            'featured_image'=>$featured_img,
            'price'=>$request->price,
            'selectmainmenu'=>$request->mainmenu,
            'selectsubmenu'=>$request->subMenuName,

            'short_intro'=>$request->short_intro,
            'discount'=>$request->discount
        ]);

        return redirect('/product-category')->with('success','Product Category Inserted Successfully');

    }

    public function showCategoryProducts($cat_id, Request $request){

        $data = $this->productCategoryRepository->getProductsByCategoryId($cat_id, $request);

        list($cat_name, $product_info, $priceFilter, $order, $sortArray, $priceFilterArray) = $data;

        return view('end-user/product-list',compact('product_info','cat_name', 'priceFilter', 'order', 'sortArray', 'priceFilterArray'));
    }

    public function productDetail($id){

        
//for review embedment
        $reviews = Review::orderby('id', 'desc')->paginate(5);

        $product_info=Product::where('id',$id)->first();

        return view('end-user/product-detail',compact('product_info', 'reviews'));

    }
    public function show()
    {
        $display = ProductCategory::orderBy('selectmainmenu', 'ASC')->orderBy('selectsubmenu', 'ASC')->orderBy('order', 'ASC')->get();

        return view('end-user.product-display', compact('display'));
    }
    public function delete($id){
        $cat = ProductCategory::findorFail($id);
        $cat->delete();
        return redirect('product-category/product-display')->with('success', 'Product Category was Successfully deleted!!!');
    }

    public function edit($id){
        $category_edit = ProductCategory::findorFail($id);
        $mainmenu = Menu::pluck('menuName','id');
     
        $submenu = SubMenu::pluck('subMenuName','id');
        return view('end-user.product-category-edit', compact('category_edit', 'mainmenu', 'submenu'));
    }

    public function update(Request $request, $id){
        $pro_cat = ProductCategory::findorFail($id);
        $pro_cat->name = $request->input('name');
        $pro_cat->order = $request->input('order');
        $pro_cat->description = $request->input('description');
        $pro_cat->selectmainmenu = $request->input('selectmainmenu');
        $pro_cat->selectsubmenu = $request->input('selectsubmenu');

        if($request->hasFile('featured_image')){
            $photo = $request->file('featured_image');
            $filename = 'cat_pic' . '-' .time() . '.' . $photo->getClientOriginalExtension();
            $location = 'images/categoryImages/';
            $request->file('featured_image')->move($location, $filename);
            $oldFilename = $pro_cat->featured_image;
            $pro_cat->featured_image = 'images/categoryImages/'.$filename;
        }
        $pro_cat->price = $request->input('price');
        $pro_cat->short_intro = $request->input('short_intro');
        $pro_cat->discount = $request->input('discount');
        $pro_cat->save();
        return redirect('/product-category/show')->with('success', 'Product Category is edited successfully!');
    }
}
