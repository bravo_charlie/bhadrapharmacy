<?php

namespace App\Http\Controllers;

use App\Repositories\CallbackRequestRepository;
use App\RequestCallback;
use Illuminate\Http\Request;
use App\Http\Requests\CreatecallbackRequest;

class RequestCallbackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $requestCallback;

    public function __construct(CallbackRequestRepository $requestCallback){
        $this->requestCallback = $requestCallback;
    }


    public function index()
    {
        return view('end-user.requestCallback');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatecallbackRequest $request)
    {
        $result = $this->requestCallback->create($request);
        return redirect()->back()->with('Success', 'Your request has been submitted.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RequestCallback  $requestCallback
     * @return \Illuminate\Http\Response
     */
    public function show(RequestCallback $requestCallback)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RequestCallback  $requestCallback
     * @return \Illuminate\Http\Response
     */
    public function edit(RequestCallback $requestCallback)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RequestCallback  $requestCallback
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RequestCallback $requestCallback)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RequestCallback  $requestCallback
     * @return \Illuminate\Http\Response
     */
    public function destroy(RequestCallback $requestCallback)
    {
        //
    }
}
