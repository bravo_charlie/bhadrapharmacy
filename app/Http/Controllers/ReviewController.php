<?php

namespace App\Http\Controllers;

use App\Repositories\ReviewRepository;
use App\Review;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Requests\ReviewStoreRequest;
use Auth;

class ReviewController extends Controller
{
    private $reviewRepository;

    public function __construct(ReviewRepository $reviewRepository){
        $this->reviewRepository = $reviewRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $product_review = Product::findorFail($id);
        $reviews = Review::orderby('id', 'desc')->paginate(5);
        return view('admin.product.review', compact('product_review', 'reviews'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    public function checkAuthentication(ReviewStoreRequest $request){
        $session_review = array(
            'value_money'=> $request->value_money,
            'quality'=> $request->quality,
            'performance' => $request->performance,
            'overall'=> $request->overall,
            'name' => $request->name,
            'email'=> $request->email,
            'recommend' => $request->recommend,
            'reviewBox' => $request->reviewBox,
            'product_id'=> $request->product_id
        );
        session()->put('session_review', $session_review);
        return $this->checkAuth();
    }


    public function checkAuth(){
        if(Auth::user()){
            $session_review = session()->get('session_review');
            $user_id = Auth::user()->id;
            array_push($session_review, $session_review['user_id'] = $user_id );
            unset($session_review[0]);
            session()->forget('session_review');
            session()->put('session_review', $session_review);
            return $this->store();
        }
        else
            return redirect('/redirect_to_register_for_auth');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $request= session()->get('session_review');
        $result = $this->reviewRepository->create($request); 
        $id = $request['product_id'];
        session()->forget('session_review');
        return redirect()->route('product-detail', ['id' => $id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $reviews = Review::all();
        return view('end-user.reviews', compact('reviews'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function edit(Review $review)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Review $review)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function destroy(Review $review)
    {
        //
    }
}