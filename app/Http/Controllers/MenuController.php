<?php

namespace App\Http\Controllers;

use App\Menu;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menu = Menu::all()->sortBy('order');
        return view('menu.index', compact('menu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('menu.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'menuName' => 'required|max:30',
            ));
        $product_cat_img = Input::file('featured_image');
        $product_cat_img_path="";
        if (!empty($product_cat_img)) {
            $destinationPathFeatured = 'images/categoryImages/';
            $product_cat_img_path=time() . "_" . $product_cat_img->getClientOriginalName();
            $product_cat_img->move($destinationPathFeatured,$product_cat_img_path);

            $featured_img=$destinationPathFeatured.$product_cat_img_path;

        }else{
            $featured_img='images/categoryImages/defaultCatImg.jpg';
        }

        $menu = new Menu();
        $menu->menuName = $request->input('menuName');
        $menu->order = $request->input('order');
        $menu->featured_image = $featured_img;
        $menu->save();
        return redirect('/menu')->with('Success, ', 'A new menu is added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function show(Menu $menu)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $menu = Menu::findorFail($id);
        return view('menu.edit', compact('menu', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $menu = Menu::find($id);
        $this->validate($request, array(
            'menuName' => 'required|max:30',
            ));

        $menu = Menu::where('id', $id)->first();
        if($request->hasFile('featured_image')){
            $photo = $request->file('featured_image');
            $filename = 'cat_pic' . '-' .time() . '.' . $photo->getClientOriginalExtension();
            $location = 'images/categoryImages/';
            $request->file('featured_image')->move($location, $filename);
            $oldFilename = $menu->featured_image;
            $menu->featured_image = 'images/categoryImages/'.$filename;
        }
        $menu->menuName = $request->input('menuName');
        $menu->order = $request->input('order');
        $menu->save();
        return redirect('/menu')->with('Success, ', 'Menu is updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $menu = Menu::findorFail($id);
        $menu->delete();
        return redirect('/menu')->with('Success', 'Menu is deleted successfully.');
    }
}
