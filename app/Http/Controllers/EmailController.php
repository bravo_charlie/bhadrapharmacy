<?php

namespace App\Http\Controllers;

use App\Mail\AdminNotification;
use App\Mail\CustomerNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class EmailController extends Controller
{
    //

    public function SendEmailToAdmin($orderDetails){
        Mail::to('rajatbhadra.1@gmail.com')->cc('enquiry@pharmacysaver.co.uk')->send(
            new AdminNotification($orderDetails)
        );

    }

    public function sendEmailToCustomer($orderDetails){
        $email=$orderDetails->delivery_email;
        Mail::to($email)->cc('rajatbhadra.1@gmail.com')->bcc('anil.qode@gmail.com')->send(
            new CustomerNotification($orderDetails)
        );
    }
}
