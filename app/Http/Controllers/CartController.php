<?php

namespace App\Http\Controllers;

use App\Product;
use App\Delivery;
use App\Coupon;
use Illuminate\Http\Request;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Validator;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $deliveries = Delivery::where('visible', 1)->get();
        $discount = getNumbers()->get('discount');
        $newSubtotal = getNumbers()->get('newSubtotal');
        $newTax = getNumbers()->get('newTax');
        $newTotal = getNumbers()->get('newTotal');        

        $deliverySelected = [];
        if ($request->session()->exists('delivery')) {
            $deliverySelected = $request->session()->get('delivery');
        }
        $coupon = '';
        if ($request->session()->exists('coupon')) {
            $couponData = $request->session()->get('coupon');
            $coupon = $couponData['coupon_code'];
        }

        if (request()->ajax()) {
            $cartItems = Cart::content();
            $cartCount = Cart::count();
            $newTotal= presentPrice($newTotal);
            return response()
                ->json(compact('cartItems', 'cartCount','discount', 'newSubtotal', 'newTax', 'newTotal', 'deliveries', 'deliverySelected', 'coupon'));
        }
        $mightAlsoLike = Product::mightAlsoLike()->get();

        return view('cart')->with([
            'mightAlsoLike' => $mightAlsoLike,
            'discount' => $discount,
            'newSubtotal' => $newSubtotal,
            'newTax' => $newTax,
            'newTotal' => $newTotal,
            'deliveries' => $deliveries,
            'deliverySelected' => $deliverySelected,
            'coupon' => $coupon
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Product $product)
    {

       $duplicates = Cart::search(function ($cartItem, $rowId) use ($product) {
            return $cartItem->id === $product->id;
        });

        if ($duplicates->isNotEmpty()) {
            return redirect()->route('cart.index')->with('success_message', 'Item is already in your cart!');
        }

        if(!empty($product->in_out_stock)) {
            return redirect()->route('cart.index')->with('success_message', 'Cannot add item to your cart! Please check if the product is in stock.');
        }

        $isGrocery = $this->checkGroceryCategory($product->productMultipleCategory);
        $taxableAmount = ($request->session()->exists('taxableAmount'))?$request->session()->get('taxableAmount'):0;
        $productPrice = empty($product->discount)?$product->price:$product->price-$product->discount;
        if(empty($isGrocery)) {
            $taxableAmount += $productPrice * $request->quantity;
            $request->session()->put('taxableAmount', $taxableAmount);
        }
        Cart::add($product->id, $product->name, $request->quantity, $productPrice, ['image'=>$product->featured_image1, 'available_quantity' =>  $product->available_quantity, 'taxable' => !$isGrocery])
            ->associate('App\Product');
        

        return redirect()->route('cart.index')->with('success_message', 'Item was added to your cart!');
    }

    public function checkGroceryCategory($productCategories) {
        foreach ($productCategories as $productCategoryObj) {
            if(!empty($productCategoryObj->productCategory->menu) && $productCategoryObj->productCategory->menu->menuName == 'Grocery') {
                return true;
            }
        }
        return false;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'quantity' => 'required|numeric|between:1,'.$request->productQuantity
        ]);

        if ($validator->fails()) {
            session()->flash('errors', collect(['Quantity must be between 1 and '.$request->productQuantity.'.']));
            return response()->json(['success' => false], 400);
        }

        if ($request->quantity > $request->productQuantity) {
            session()->flash('errors', collect(['We currently do not have enough items in stock.']));
            return response()->json(['success' => false], 400);
        }

        $taxableAmount = ($request->session()->exists('taxableAmount'))?$request->session()->get('taxableAmount'):0;
        $cartProduct = Cart::get($id);
        if($cartProduct->options['taxable']) {
            $taxableAmount -= $cartProduct->qty*$cartProduct->price;
            $taxableAmount += $request->quantity*$cartProduct->price;
            $request->session()->put('taxableAmount', $taxableAmount);
        }

        Cart::update($id, $request->quantity);
        session()->flash('success_message', 'Quantity was updated successfully!');
        return response()->json(['success' => true]);
    }

    /**
     * Fuction to update delivery price.
     */
    public function updateDelivery(Request $request)
    {

        $data = $request->all();
        if (request()->ajax()) { 
            if ($request->session()->exists('delivery')) {
                $request->session()->forget('delivery');
            }
            $request->session()->put('delivery', $data['delivery']);
            return response()->json(['success' => true]);
        }
        
    }

    /**
     * Fuction to update delivery price.
     */
    public function updateCoupon(Request $request)
    {
        if (request()->ajax()) { 
            $data = $request->all();
            $coupon = Coupon::where('coupon_code', '=', $data['coupon'])
                              ->where('visible', '=', 1)->first();
            if(empty($coupon)) {
                return response()->json(['status' => 'error', 'msg' => 'Invalid Coupon']);
            }

            if($coupon->coupon_use_type != 'Unlimited' && 
                (int)$coupon->coupon_applicable_times <= (int)$coupon->coupon_applied_times) {
                return response()->json(['status' => 'error', 'msg' => 'Coupon Expired']);
            }

            if($coupon->coupon_type=='Delivery' && !$request->session()->exists('delivery')) {
                return response()->json(['status' => 'error', 'msg' => 'Please select Delivery option to use this coupon']);
            }

            $delivery = ($request->session()->exists('delivery'))?$request->session()->get('delivery'):null;

            if(!empty($coupon->minimum_amount)) {
                if(!$this->checkForMinimumAmount($coupon, $delivery)) {
                    $couponType = ($coupon->coupon_type=='Discount')?'total product':'delivery';
                    return response()->json(['status' => 'error', 'msg' => 'Use can only use this coupon for '.$couponType.' amount greater than '.$coupon->minimum_amount]);
                }
            }

            if ($request->session()->exists('coupon')) {
                $request->session()->forget('coupon');
            }
            $request->session()->put('coupon', $coupon->toArray());
            return response()->json(['status'=> 'success','success' => true]);
        }
        
    }

    private function checkForMinimumAmount($coupon, $delivery = null) {
        
        if($coupon->coupon_type == 'Discount') {
            if((float)Cart::subtotal() < (float)$coupon->minimum_amount) {
                return false;
            }
        }
        if ($coupon->coupon_type == 'Delivery') {
            if((float)$delivery['price'] < (float)$coupon->minimum_amount) {
                return false;
            }
        }
        return true;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $cartProduct = Cart::get($id);

        $taxableAmount = ($request->session()->exists('taxableAmount'))?$request->session()->get('taxableAmount'):0;

        if($cartProduct->options['taxable']) {
            $taxableAmount -= $cartProduct->price * $cartProduct->qty;
            $request->session()->put('taxableAmount', $taxableAmount);
        }

        Cart::remove($id);
        if (request()->ajax()) {
            $request->session()->flash('success_message', 'Item has been removed!');
            return response()->json(['success' => true]);
        }

        return back()->with('success_message', 'Item has been removed!');
    }
}
