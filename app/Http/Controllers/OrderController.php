<?php

namespace App\Http\Controllers;

use App\Order;
use App\OrderProduct;
use Illuminate\Http\Request;
use App\User;
use Auth;
use App\Product;
use Gloudemans\Shoppingcart\Facades\Cart;
use Response;
use Exception;
use App\Mail\DispatchEmail;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }

    public function dispatchOrder(Request $request, Order $order)
    {

        try {
            $order->dispatch_id = $request->dispatch_id;
            $order->dispatch_email = $request->dispatch_email;
            $order->save();
            Mail::to($order->dispatch_email)->send(new DispatchEmail($order));
        } catch(Exception $e) {
            return redirect()->back()->with('error', "Error in saving dispatch info.");
        }

        return redirect()->back()->with('success_message', 'Dispatch info saved successfully.');


    }

    public function reOrder(Request $request) {

        $order = Order::with(['orderProduct', 'orderProduct.product'])->where('id',$request->id)->first();
        foreach ($order->orderProduct as $orderProduct) {
            $product = $orderProduct->product;
            Cart::add($product->id, $product->name, $orderProduct->quantity, $product->price, ['image'=>$product->featured_image1, 'availableQuantity'=> (int)$product->available_quantity])
            ->associate('App\Product');
        }

        return redirect()->action('CartController@index')->with('success_message', 'Reordered items are added to your cart!');

    }

    public function orderhistory()
    {
        $user = Auth::user();

        if($user->hasRole('super-admin')) {
            $orders = Order::with(['delivery','orderProduct', 'orderProduct.product'])->orderBy('created_at', 'DESC')->get();
            return view('admin.order', compact('orders'));
        } else {
            $orders = Order::with(['delivery','orderProduct', 'orderProduct.product'])->where('user_id', $user->id)->orderBy('created_at', 'DESC')->get();
            return view('users.order-history', compact('orders'));
        } 
    }



    public function export()
    {
        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=orders.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        $user = Auth::user();

        if($user->hasRole('super-admin')) {
            $params = Order::with(['delivery','orderProduct', 'orderProduct.product'])->orderBy('created_at', 'DESC')->get();   
          
        } else {
            $params = Order::with(['delivery','orderProduct', 'orderProduct.product'])->where('user_id', $user->id)->orderBy('created_at', 'DESC')->get();
          
        }
        
        $columns = array('S.N','Order Id', 'Date of Order', 'Product details', 'Tax', 'Amount', 'Customer Name', 'Phone Number', 'Email', 'Delivery Address');


        $callback = function() use ($params, $columns)
        {
            $counter = 0;
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach($params as $orders) {
                $delivery_address = 'City:- '.$orders->delivery_city.', '.'Address:- '.$orders->delivery_address.', '.'Postal Code:- '.$orders->delivery_postalcode;

                $products = $orders->orderProduct;
                $orderedProductDetails = "";
                $orderedProduct = array($orderedProductDetails);
                    
                foreach($products as $product) 
                    {
                        $orderedProductDetails = $product->product['name'];
                          $quantityPotency = '( Quantity: '.$product->quantity . ' ) ( Potency : ' . $product->product['potency'] . ' )';
                           array_push($orderedProduct, $orderedProductDetails, $quantityPotency); 
                    }
                   
                $proString = implode("=>", $orderedProduct);

                fputcsv(
                    $file, 
                    array(
                        ++$counter,
                        $orders->id,
                        $orders->created_at,
                        $proString, 
                        $orders->billing_tax, 
                        $orders->billing_total, 
                        $orders->delivery_name, 
                        $orders->delivery_phone, 
                        $orders->delivery_email, 
                        $delivery_address
                    )
                );
            }
            fclose($file);
        };
        return Response::stream($callback, 200, $headers);
    }
}
