<?php

namespace App\Exports;

use App\OrderDetails;
use Maatwebsite\Excel\Concerns\FromCollection;

class OrdersExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return view('admin.previewpdf');
    }
}
