<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuggestandWin extends Model
{
    protected $fillable = [
    	'suggestion', 'title', 'firstname', 'lastname', 'email', 'confirm-email'
    ];
}
