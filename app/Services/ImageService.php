<?php

namespace App\Services;

/**
 * Image Service to save/upload images
 */
class ImageService
{
	/**
	 * Save image on the specified location.
	 * @param type $photo 
	 * @param type|string $path 
	 * @return type
	 */
	public function saveImage($photo, $path='images/')
	{
		$filename = 'slide' . '-' .time() . '.' . $photo->getClientOriginalExtension();
		$location = public_path('storage/'.$path);
		$photo->move($location, $filename);
		return $filename;
	}
}
