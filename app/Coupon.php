<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Coupon extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $fillable = [
    	'coupon_type', 'coupon_code', 'discount_type', 
    	'discount_value', 'minimum_amount', 'coupon_use_type',
    	'coupon_applicable_times', 'coupon_applied_times', 'visible'
    ];

    /**
     * Ordere associated with the Delivery
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function order()
    {
        return $this->hasMany(Order::class);
    }
}
