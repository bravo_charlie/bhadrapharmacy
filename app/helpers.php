<?php

use Carbon\Carbon;

function asDollars($value) {
  return '£' . number_format($value, 2);
}

function presentPrice($price)
{
    return asDollars($price);
}

function presentDate($date)
{
    return Carbon::parse($date)->format('M d, Y');
}

function setActiveCategory($category, $output = 'active')
{
    return request()->category == $category ? $output : '';
}

function productImage($path)
{
    return $path && file_exists($path) ? asset($path) : asset('images/not-found.jpg');
}

function getDiscountAmountFrom() {
    $coupon = session()->get('coupon');
    if($coupon['coupon_type'] == 'Delivery') {
        $delivery = session()->get('delivery');
        return (int)$delivery['price'];
    } else if($coupon['coupon_type'] == 'Discount') {
        return Cart::subtotal();
    }
    return 0;
}

function getDiscountAmount() {
    if(!empty(session()->get('coupon'))) {
        $coupon = session()->get('coupon');
        if($coupon['discount_type'] == 'Rate') {
            return $coupon['discount_value'];
        }
        if($coupon['discount_type'] == 'Percentage') {
            $discountFrom = getDiscountAmountFrom();
            return $discountFrom*((float)$coupon['discount_value']/100);
        }
    }
    return 0;
}

function getDiscountedAmount($discountType) {
    $coupon = session()->get('coupon');
    if(!empty($coupon)) {
        if($discountType == 'Discount') {
            if($coupon['coupon_type'] == 'Discount') {
                return getDiscountAmount();
            }
        }

        if($discountType == 'Delivery') {
            if($coupon['coupon_type'] == 'Delivery') {
                return getDiscountAmount();
            }
        }
    }
    return 0;
}

function getNumbers()
{
    $tax = config('cart.tax') / 100;
    $discount = getDiscountedAmount('Discount');
    $code = session()->get('coupon')['coupon_code'] ?? null;
    $delivery = session()->get('delivery') ?? [];

    $newSubtotal = ((float)Cart::subtotal() - (float)$discount);
    if ($newSubtotal < 0) {
        $newSubtotal = 0;
    }
    $taxableAmount = session()->get('taxableAmount');
    $newTax = $taxableAmount * $tax;

    $deliveryDiscount = getDiscountedAmount('Delivery');
    $newDelivery = (!empty($delivery))?(float)$delivery['price']-$deliveryDiscount:0;

    $newTotal = $newSubtotal + $newTax;
    if(!empty($delivery)) {
        $newTotal += $newDelivery;
    }

    return collect([
        'tax' => $tax,
        'discount' => empty($discount)?$deliveryDiscount:$discount,
        'code' => $code,
        'newSubtotal' => $newSubtotal,
        'newTax' => $newTax,
        'newTotal' => $newTotal,
    ]);
}

function getStockLevel($quantity)
{
    if ($quantity > setting('site.stock_threshold', 5)) {
        $stockLevel = '<div class="badge badge-success">In Stock</div>';
    } elseif ($quantity <= setting('site.stock_threshold', 5) && $quantity > 0) {
        $stockLevel = '<div class="badge badge-warning">Low Stock</div>';
    } else {
        $stockLevel = '<div class="badge badge-danger">Not available</div>';
    }

    return $stockLevel;
}
