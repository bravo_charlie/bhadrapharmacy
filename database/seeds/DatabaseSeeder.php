<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        $this->createUser();
        // this can be done as separate statements
        $role = Role::create(['name' => 'super-admin']);
        $admin = User::where('email','admin@admin.com')->first();
        $admin->assignRole($role->name);

        // or may be done by chaining
        $role = Role::create(['name' => 'customer']);
        $customer = User::where('email','customer@customer.com')->first();
        $customer->assignRole($role->name);



    }

    /**
     * Create default users 
     *
     * @param $role
     */
    private function createUser()
    {
    	$users = array(
            ['name'=>'Admin', 'email'=>'admin@admin.com', 'password'=>bcrypt('password123')],
    		['name'=>'Customer', 'email'=>'customer@customer.com', 'password'=>bcrypt('password123')]
    	);
        foreach ($users as $user) {
            $user = User::create($user);
        }
    }
}
