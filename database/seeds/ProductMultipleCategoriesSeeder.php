<?php

use Illuminate\Database\Seeder;
use App\Product;
use App\ProductMultipleCategory;

class ProductMultipleCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = Product::where('product_category_id','!=', Null)->get();
        foreach ($products as $product) {
        	$multipleCategory = ProductMultipleCategory::where('product_id',$product->id)
        												->where('product_category_id', $product->product_category_id)
        												->first();
        	if(empty($multipleCategory)) {
        		ProductMultipleCategory::create([
        			'product_id'=> $product->id,
        			'product_category_id'=> $product->product_category_id
        		]);
        	}
        }
    }
}
