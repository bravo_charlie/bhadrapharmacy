<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name')->nullable();
            $table->text('description')->nullable();
            $table->integer('product_category_id')->unsigned();
            $table->text('featured_image1')->nullable();
            $table->text('featured_image2')->nullable();
            $table->text('featured_image3')->nullable();
            $table->text('price')->nullable();
            $table->text('potency')->nullable();
            $table->text('size')->nullable();
            $table->text('rewards')->nullable();
            $table->text('short_intro')->nullable();
            $table->text('discount')->nullable();
            $table->boolean('in_out_stock')->nullable();
            $table->double('available_quantity')->nullable();
            $table->text('sku')->nullable();
            $table->text('directions')->nullable();
            $table->text('ingredients')->nullable();
            $table->text('advisory_information')->nullable();
            $table->text('remember_to')->nullable();
            $table->text('meta_title')->nullable();
            $table->text('meta_description')->nullable();
            $table->text('h1')->nullable();
            $table->text('h2')->nullable();
            $table->text('h3')->nullable();
            $table->timestamps();

            $table->foreign('product_category_id')->references('id')->on('product_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
