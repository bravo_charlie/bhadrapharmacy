

<div id="mobileMenu">
  <!-- <nav class="navbar navbar-expand-md navbar-light bg-light"> -->
  <div class="container">
    <div class="row">
      <div class="col-xs-1 navbar navbar-expand-md nav-chemist">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mobile_Menu" aria-controls="mobile_Menu" aria-expanded="false" aria-label="Toggle navigation">
          <i class="fa fa-bars"></i>
        </button>
        <nav class="collapse navbar-collapse" id="mobile_Menu" >
          <ul class="dropdown-menu navbar-nav ml-auto">
            @foreach($main_menu as $menu)
            <li class="dropdown-submenu">
              <a tabindex="-1" href="/main-menu/{{$menu->id}}">{{ $menu->menuName }}</a>
              <i class="material-icons show-sub-menu">keyboard_arrow_right</i>
              <ul class="dropdown-menu mobile_sub_menu">
                @foreach( $sub_menu as $submenu )
                @if($menu->id == $submenu->mainmenuid)
                <li class="dropdown-submenu">
                  <a class="test" href="/sub-menu/{{$submenu->id}}">{{ $submenu->subMenuName}}<span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    @foreach( $categories as $category)
                    @if($submenu->id == $category->selectsubmenu)
                      <li><a href="/products/{{$category->id}}">{{ $category->name}}</a></li>
                    @endif
                    @endforeach
                  </ul>
                </li>
                @endif
                @endforeach
              </ul>
            </li>
            @endforeach
          </ul>
        </nav>
      </div>
           <div class="col-xs-1">
        @if (Auth::check())
              <a href="/home"><i class="fa fa-user"></i></i></a>
            @else
              <a href="/login"><i class="fa fa-user"></i></i></a>
            @endif
      </div>
      <div class="col-xs-8">
        <a class="navbar-brand" href="/"><img src="/images/homepage-images/nutritionplanet.jpg" class="mobileMenu"></a>
      </div>
      <div class="col-xs-1">
        
        

        <span data-toggle="modal" data-target="#showMap">
          <i class="fa fa-map-marker-alt" aria-hidden="true"></i>
        </span>

        <!-- The modal -->
        <div class="modal fade" id="showMap" tabindex="-1" role="dialog" aria-labelledby="modalLabelSmall" aria-hidden="true">
          <div class="modal-dialog modal-sm">
            <div class="modal-content">

              <div class="modal-header">
                <h4 class="modal-title" id="modalLabelSmall">Find us Now</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
              </div>

              <div class="modal-body">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3532.4622609455355!2d85.32120341453825!3d27.703010332290464!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb19a85bffffff%3A0x358c8c57ee9eb1ad!2sNepgeeks+Technology!5e0!3m2!1sen!2snp!4v1549363199549" width="100%" height="auto" frameborder="0" style="border:0" allowfullscreen></iframe>
              </div>

            </div>
          </div>
        </div>
      </div>
      <div class="col-xs-1">
        <cart-list></cart-list>
      </div>
    </div>
    <div class="row deliveryliner">
      <p>
        Fast Delivery with <strong>Premium Delivery</strong>
      </p>
    </div>
    <div class="row">

        <form action="/queries/search" method="POST" role="search" id="search-bar">
              {{ csrf_field() }}
              <div class="input-group">
                  <input type="text" class="form-control" name="search" placeholder="Search for a product" id="searchBar"> 
                  <span class="input-group-btn">
                      <button type="submit" class="btn btn-default" onclick="openInput(event)">
                          <i class="fa fa-search"></i>
                      </button>
                  </span>
              </div>
          </form>

    </div>
  </div>
</div>

  <div class="top-header" id="top-header">
    <div class="container">
    <div class="row">


    <div class="col-sm-3">
      <a class="navbar-brand" href="/"><img src="/images/homepage-images/nutritionplanet.jpg" style="width: 100%;"></a>
    </div>
      <div class="col-sm-9">

      <div class="row cart-search-section">
        <div class="col-sm-10">
          <form action="/queries/search" method="POST" role="search" id="search-bar">
              {{ csrf_field() }}
              <div class="input-group">
                  <input type="text" class="form-control" name="search" placeholder="Search for a product"> 
                  <span class="input-group-btn">
                      <button type="submit" class="btn btn-default">
                          <i class="fa fa-search"></i>
                      </button>
                  </span>
              </div>
          </form>
        </div>
        <div class="col-sm-2">
          <cart-list></cart-list>
        </div>
      </div>
    </div>
    </div>
    </div>
  </div>
	<div class="main-menu">
    <div class="container p-0">
      <div class="row main-menu d-flex align-items-center">

        <div class="col-sm-10">    
          <nav class="navbar navbar-expand-lg navbar-light pull-right">
            
            <div class="" onmouseleave="closeAll()" id="navbarSupportedContent">
              <header class="main-header">

                <div class="tab">
                  @foreach($menus as $menu)
                  <a class="tablinks " href="/main-menu/{{ $menu['id']}}" onmouseover="openTab(event, `menuTab{{ $menu['id'] }}`)">{{ $menu['menuName'] }}</a>
                  @endforeach
                </div>

                @foreach($menus as $menu)
                <div class="tabcontent" id="menuTab{{ $menu['id'] }}" 
                  onmouseleave="closeTab(`menuTab{{ $menu['id'] }}`)" style="display: none;">
                  <div class="container px-0 py-4">
                    <div class="sub-content">
                      
                      <div class="row">
                        @php $count = 0; @endphp
                        @foreach( $menu['contents'] as $key => $contents )
                        
                        <div class="col-sm-2 third-level mb-5">
                          @foreach($contents as $index => $content)
                            @if($content['type'] == 'sub-menu')
                              @if($count != 0)
                                <p class="mt-4"></p>
                              @endif
                              <a href="/sub-menu/{{$content['id']}}"><h5 style="color: #2170b5;font-weight: bold;margin-bottom: 10px;">{{ $content['name']}}</h5></a>
                            @else
                              <p class="menu-links mb-2">
                                  <a href="/products/{{$content['id']}}" style="text-transform: capitalize;display: block;padding-bottom: 0px;">{{ $content['name'] }}</a>
                              </p>
                            @endif
                            @php $count++; @endphp
                          @endforeach
                          
                          
                        </div>
                        @endforeach
                      </div>
                    </div>
                    
                  </div>
                </div>
                @endforeach
                
      </header>
          
            </div>
          </nav>
        </div>
        <div class="col-sm-2" >
          <div class="right-side">
            @if (Auth::check())
              <a href="/home"><i class="fas fa-user"></i></i></a>
            @else
              <a href="/login"><i class="fas fa-user"></i></i></a>
            @endif
            
          </div>
        </div>
      </div>
    </div>
  </div>
 