@section('title', 'Terms & Conditions')
@extends('layouts.admin')

@section('content')
<div class="container-fluid">
@if($errors->any())
	<div class="alert alert-danger">
		<ul>
			@foreach($errors->all() as $error)
				<li>{{$error}}</li>
			@endforeach
		</ul>
	</div>
@endif
<div class="row">
	<div class="col-sm-3 customer_sidebar">
		@include('layouts.sidebar')
	</div>
	<div class="col-sm-9 customer_playzone center">
		<div class="row">
			<div class="col-sm-12">
				<h4>Terms & Conditions</h4>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				@if(empty($data))
					{!! Form::open(['url' => "/save-agreement",'enctype'=>'multipart/form-data', 'method' => 'POST', 'class' => 'form-horizontal']) !!}
				@else
					{!! Form::open(['url' => "/save-agreement/".$data->id,'enctype'=>'multipart/form-data', 'method' => 'PUT', 'class' => 'form-horizontal']) !!}
				@endif
				<text-editor label="Terms & Conditions" name="contents" value="{{ !empty($data)?$data->contents:'' }}"></text-editor>

				<div class="row">
					<div class="col-md-2 col-sm-2 col-xs-12 pl-25 mt-3">
					    {{ Form::submit('Save', ['class' => 'btn btn-primary btn-lg btn-block']) }}
					</div>
				</div>

				{!! Form::hidden('type', 'terms') !!}
				 {!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
</div>
@endsection
