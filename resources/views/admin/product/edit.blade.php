@section('title', 'Edit Product')
@extends('layouts.admin')
@push('mystyles')
    <link rel="stylesheet" type="text/css" href="{{ url('css/app.css') }}">
    <link href="{{ url('/css/clientdashboard.css') }}" rel="stylesheet">

    <style type="text/css">
        .roles-list-title{
            top: -6px !important;
            margin-bottom: 8px;
            margin-left:12px;
        }
        .inventrary_list {
            /*box-shadow: 0 0 20px rgba(0, 0, 0, 0.2);*/
            border-radius: 10px;
            background-color: #ffffff !important;
                min-height: 556px;
        }
        .ptpl{
                padding-top: 15px;
    padding-left: 29px;
        }
        .create-form-group select{
            width: 100%;
            margin: 14px 0 0 0;
        }
        .card_img_box {
            padding: 40px 20px 36px !important;
        }
        .dealer_setting_img img, .dealer_logo_url {
            max-width: 70px !important;
            /* width: 100%; */
        }
        .card_img_box h6 {
            margin-top: 5px;
            color: #999999;
            font-weight: 400 !important;
            font-size: 18px;
            padding-top: 34px !important;
        }
        select {
            width: 100%;
            height: 50px !important;
            border-radius: 10px !important;
            border: 1px solid #cccccc;
            background: url(/img/down_arrow.png) 98.5% no-repeat;
            background-color: #ffffff;
            -webkit-appearance: none;
            float: right;
            font-size: 18px !important;
            color: #999 !important;
        }
    </style>
@endpush
@section('content')

  <div class="container-fluid container-wrapper mt-19">
        <section class="p_15 mb-63 dashboard_inventary">
            <div class="row">
                <div class="col-sm-3">
                    @include('layouts.sidebar')
                </div>
                <div class="col-sm-9">
                    <div class="row">
                        <div class="col-sm-11">
                            <div class="roles-list-title">
                                <h4>Edit Product</h4>
                            </div>
                        </div>
                        
                        <div class="clearfix"></div>
                    </div>
                    <div class="row ptpl">
                        <div class="col-sm-12 inventrary_list">
                            <div class="customer_vehicle_form">
                                {!! Form::open(['url' => "/products/edit/$product_edit->id",'enctype'=>'multipart/form-data','file'=>true,'method' => 'POST', 'class' => 'form-horizontal login-form']) !!}
                                {{ csrf_field() }}
                                    <div class="row top_50">
                                       
                                        <div class="col-sm-12 create-form-group pl-30">
                                            <div class="row mt-6n margin-bottom-10">
                                               <div class="col-sm-6">
                                                {!! Form::label('Product Name') !!}
                                                    {!! Form::text('name',$product_edit->name, ['class' => 'form-control','placeholder' => 'Product Name', 'required' ]) !!}
                                                </div>
                                                <div class="col-sm-6 pl-25">
                                                    {!! Form::label('Brief Intro') !!}
                                                    {!! Form::text('short_intro',$product_edit->short_intro, ['class' => 'form-control','placeholder' => 'Brief Intro']) !!}
                                                </div>

                                            </div>

                                            <div class="row mt-24 margin-bottom-10">
                                                <div class="col-sm-6 pl-25">
                                                    {!! Form::label('Product Categories') !!}
                                                    <product-categories-select 
                                                    :id='`categories`' 
                                                    :name="`categories[]`" 
                                                    :selected="{{ json_encode($selected_categories) }}"
                                                    :items="{{ json_encode($product_cat) }}"></product-categories-select>
                                                 </div>
                                                
                                                <div class="col-sm-6 pl-25">
                                                    {!! Form::label('Price') !!}
                                                    <span class="currency-input">
                                                        {!! Form::text('price', $product_edit->price, ['class' => 'form-control','placeholder' => 'Price', 'required']) !!}
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="row mt-24 margin-bottom-10">
                                                <div class="col-sm-6">
                                                    {!! Form::label('Potency') !!}
                                                   {!! Form::text('potency', $product_edit->potency, ['class' => 'form-control','placeholder' => 'Potency']) !!}
                                                </div>
                                                <div class="col-sm-6 pl-25">
                                                    {!! Form::label('Size') !!}
                                                    {!! Form::text('size', $product_edit->size, ['class' => 'form-control','placeholder' => 'Size']) !!}
                                                </div>
                                            </div>
                                            <div class="row mt-24 margin-bottom-10">
                                                <div class="col-sm-6">
                                                    {!! Form::label('SKU') !!}
                                                   {!! Form::text('sku', $product_edit->sku, ['class' => 'form-control','placeholder' => 'SKU']) !!}
                                                </div>
                                                <div class="col-sm-6 pl-25">
                                                    {!! Form::label('Discount') !!}
                                                    <span class="currency-input">
                                                        {!! Form::text('discount',$product_edit->discount, ['class' => 'form-control','placeholder' => 'Discount']) !!}
                                                    </span>
                                                </div>
                                            </div>

                                               <div class="row mt-24 margin-bottom-10">
                                                <div class="col-sm-6">
                                                    {!! Form::label('Stock Status') !!}
                                                  {!! Form::select('in_out_stock', $stock,  $product_edit->in_out_stock, ['class' => 'form-control','placeholder' => 'Select Stock Status', 'required']); !!}
                                                </div>
                                                <div class="col-sm-6 pl-25">
                                                    {!! Form::label('Available Quantity') !!}
                                                    {!! Form::text('available_quantity',$product_edit->available_quantity , ['class' => 'form-control','placeholder' => 'Available Quantity']) !!}
                                                </div>
                                            </div>

                                            <div class="row" style="margin-top:25px;">

                                            <div class="col-lg-4 picture-container dealer_setting_img">
                                            <div class="card_img_box picture top_10">
                                            
                                                <img src="{{ url($product_edit->featured_image1) }}" class="dealer_logo_url" height="300px">
                                                <input type="file" name="featured_image1" class="card_img_file">
                                                <h6>Upload Photo</h6>
                                            </div>
                                         
                                        </div>

                                         <div class="col-lg-4 picture-container dealer_setting_img">
                                            <div class="card_img_box picture top_10">
                                            
                                                <img src="{{ url($product_edit->featured_image2) }}" class="dealer_logo_url" height="300px">
                                                <input type="file" name="featured_image2" class="card_img_file">
                                                <h6>Upload Photo</h6>
                                            </div>
                                         
                                        </div>

                                         <div class="col-lg-4 picture-container dealer_setting_img">
                                            <div class="card_img_box picture top_10">
                                            
                                                <img src="{{ url($product_edit->featured_image3) }}" class="dealer_logo_url" height="300px">
                                                <input type="file" name="featured_image3" class="card_img_file">
                                                <h6>Upload Photo</h6>
                                            </div>
                                         
                                        </div>
                                            </div>

                                          <div class="row mt-24 margin-bottom-10">
                                            <div class="col-sm-6">
                                                <text-editor label="Ingredients" name="ingredients" value="{{ $product_edit->ingredients }}"></text-editor>
                                            </div>
                                            <div class="col-sm-6">
                                                <text-editor label="Usage/Instruction" name="directions" value="{{ $product_edit->directions }}"></text-editor>
                                                 
                                            </div>

                                        </div>

                                         <div class="row mt-24 margin-bottom-10">
                                             <div class="col-sm-6 pl-25">
                                                <text-editor label="Warnings" name="warnings" value="{{ $product_edit->warnings }}"></text-editor>
                                             </div>
                                            <div class="col-sm-6">
                                                <text-editor label="Description" name="description" value="{{ $product_edit->description }}"></text-editor>
                                            </div>

                                        </div>

                                            <div class="row mt-24">
                                                <div class="col-sm-6">
                                                    <a href="/products-show" class="btn btn-danger btn-lg btn-block">Cancel</a>
                                                </div>
                                                <div class="col-sm-6 pl-25">
                                                    {{ Form::submit('Save', ['class' => 'btn btn-primary btn-lg btn-block']) }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                {!! Form::close() !!}

                            </div><!-- .customer_vehicle_form -->
                        </div>
                    </div>
                </div>

            </div><!-- .row -->
        </section>
    </div><!-- .container-fluid -->
@endsection
@push('myScripts')
    <script type="text/javascript" src="{{ url('/js/jquery.min.js') }}"></script>
   <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
@endpush