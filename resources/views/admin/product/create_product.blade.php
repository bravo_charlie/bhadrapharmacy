@section('title', 'Create Product')
@extends('layouts.admin')
@push('mystyles')
    <link rel="stylesheet" type="text/css" href="{{ url('css/app.css') }}">
    <link href="{{ url('/css/clientdashboard.css') }}" rel="stylesheet">
    

    <style type="text/css">
        .roles-list-title{
            top: -6px !important;
            margin-bottom: 8px;
            margin-left:12px;
        }
        .inventrary_list {
            /*box-shadow: 0 0 20px rgba(0, 0, 0, 0.2);*/
            border-radius: 10px;
            background-color: #ffffff !important;
                min-height: 556px;
        }
        .ptpl{
                padding-top: 15px;
    padding-left: 29px;
        }
        .create-form-group select{
            width: 100%;
            margin: 14px 0 0 0;
        }
        .card_img_box {
            padding: 40px 20px 36px !important;
        }
        .dealer_setting_img img, .dealer_logo_url {
            max-width: 70px !important;
            /* width: 100%; */
        }
        .card_img_box h6 {
            margin-top: 5px;
            color: #999999;
            font-weight: 400 !important;
            font-size: 18px;
            padding-top: 34px !important;
        }
        select {
            width: 100%;
            height: 50px !important;
            border-radius: 10px !important;
            border: 1px solid #cccccc;
            background: url(/img/down_arrow.png) 98.5% no-repeat;
            background-color: #ffffff;
            -webkit-appearance: none;
            float: right;
            font-size: 18px !important;
            color: #999 !important;
        }
    </style>
@endpush
@section('content')

  <div class="container-fluid container-wrapper mt-19">
        <section class="p_15 mb-63 dashboard_inventary">
            <div class="row">
                <div class="col-sm-3">
                    @include('layouts.sidebar')
                </div>
                <div class="col-sm-9">
                    <div class="row">
                        <div class="col-lg-11">
                            <div class="roles-list-title">
                                <h4>Create Product</h4>
                            </div>
                        </div>
                        
                        <div class="clearfix"></div>
                    </div>
                    <div class="row ptpl">
                        <div class="col-lg-12 inventrary_list">
                            <div class="customer_vehicle_form">
                                {!! Form::open(['url' => "/product/",'enctype'=>'multipart/form-data','file'=>true,'method' => 'POST', 'class' => 'form-horizontal login-form']) !!}
                                {{ csrf_field() }}
                                    <div class="row top_50">
                                       
                                        <div class="col-sm-12 create-form-group pl-30">
                                            <div class="row mt-6n margin-bottom-10">
                                               <div class="col-sm-6">
                                                    {!! Form::label('Product Name') !!}
                                                    {!! Form::text('name','', ['class' => 'form-control','placeholder' => 'Product Name', 'required']) !!}
                                                    
                                                </div>
                                                <div class="col-sm-6 pl-25">
                                                     {!! Form::label('Brief Intro') !!}
                                                    {!! Form::text('short_intro','', ['class' => 'form-control','placeholder' => 'Brief Intro']) !!}
                                                </div>

                                            </div>

                                            <div class="row mt-24 margin-bottom-10">
                                                <div class="col-sm-6 pl-25">
                                                     {!! Form::label('Product Categories') !!}
                                                    <product-categories-select :id='`categories`' :name="`categories[]`" :items="{{ json_encode($product_cat) }}"></product-categories-select>
                                                 </div>
                                                
                                                <div class="col-sm-6 pl-25">
                                                     {!! Form::label('Price') !!}
                                                   <span class="currency-input">
                                                    {!! Form::text('price','', ['class' => 'form-control','placeholder' => 'Price', 'required']) !!}
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="row mt-24 margin-bottom-10">
                                                <div class="col-sm-6">
                                                     {!! Form::label('Potency') !!}
                                                   {!! Form::text('potency','', ['class' => 'form-control','placeholder' => 'Potency']) !!}
                                                </div>
                                                <div class="col-sm-6 pl-25">
                                                     {!! Form::label('Size') !!}
                                                    {!! Form::text('size','', ['class' => 'form-control','placeholder' => 'Size']) !!}
                                                </div>
                                            </div>
                                            <div class="row mt-24 margin-bottom-10">
                                                <div class="col-sm-6">
                                                     {!! Form::label('SKU') !!}
                                                   {!! Form::text('sku','', ['class' => 'form-control','placeholder' => 'SKU']) !!}
                                                </div>
                                                <div class="col-sm-6 pl-25">
                                                    {!! Form::label('Discount') !!}
                                                    <span class="currency-input">
                                                        {!! Form::text('discount','', ['class' => 'form-control','placeholder' => 'Discount']) !!}
                                                    </span>
                                                </div>
                                            </div>

                                               <div class="row mt-24 margin-bottom-10">
                                                <div class="col-sm-6">
                                                     {!! Form::label('Stock Status') !!}
                                                  {!! Form::select('in_out_stock', $stock, null, ['class' => 'form-control','placeholder' => 'Select Stock Status', 'required']); !!}
                                                </div>
                                                <div class="col-sm-6 pl-25">
                                                     {!! Form::label('Available Quantity') !!}
                                                    {!! Form::text('available_quantity','', ['class' => 'form-control','placeholder' => 'Available Quantity']) !!}
                                                </div>
                                            </div>

                                            <div class="row" style="margin-top:25px;">

                                            <div class="col-sm-4 picture-container dealer_setting_img">
                                            <div class="card_img_box picture top_10">
                                            <h6 class="upload">Upload 1st Photo</h6>
                                               <!--  <img src="{{ url('images/icons/card_upload.svg') }}" class="dealer_logo_url"> -->
                                                <input type="file" name="featured_image1" class="card_img_file">
                                                
                                            </div>
                                         
                                        </div>

                                         <div class="col-sm-4 picture-container dealer_setting_img">
                                            <div class="card_img_box picture top_10">
                                             <h6 class="upload">Upload 2nd photo</h6>
                                               <!--  <img src="{{ url('images/icons/card_upload.svg') }}" class="dealer_logo_url"> -->
                                                <input type="file" name="featured_image2" class="card_img_file">
                                               
                                            </div>
                                         
                                        </div>

                                         <div class="col-sm-4 picture-container dealer_setting_img">
                                            <div class="card_img_box picture top_10">
                                             <h6 class="upload">Upload 3rd Photo</h6>
                                               <!--  <img src="{{ url('images/icons/card_upload.svg') }}" class="dealer_logo_url"> -->
                                                <input type="file" name="featured_image3" class="card_img_file">
                                               
                                            </div>
                                         
                                        </div>
                                            </div>

                                        <div class="row mt-24 margin-bottom-10">
                                                <div class="col-sm-6">
                                                    <text-editor label="Ingredients" name="ingredients" value=""></text-editor>
                                                </div>
                                                <div class="col-sm-6">
                                                    <text-editor label="Usage/Instruction" name="directions" value=""></text-editor>
                                                </div>
                                            </div>

                                             <div class="row mt-24 margin-bottom-10">
                                                <div class="col-sm-6">
                                                   <text-editor label="Warnings" name="warnings" value=""></text-editor>
                                                </div>
                                                <div class="col-sm-6">
                                                   <text-editor label="Description" name="description" value=""></text-editor>
                                                </div>

                                            </div>
                                            
                                            <div class="row mt-24">
                                                <div class="col-sm-6">
                                                    <a href="/products-show" class="btn btn-danger btn-lg btn-block">Cancel</a>
                                                </div>
                                                <div class="col-sm-6 pl-25">
                                                    {{ Form::submit('Save', ['class' => 'btn btn-primary btn-lg btn-block']) }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                {!! Form::close() !!}

                            </div><!-- .customer_vehicle_form -->
                        </div>
                    </div>
                </div>

            </div><!-- .row -->
        </section>
    </div><!-- .container-fluid -->
@endsection
@push('myScripts')
    <script type="text/javascript" src="{{ url('/js/jquery.min.js') }}"></script>
   
@endpush
