
@extends('layout')

@section('content')
<div class="container-fluid product_review">
	@if ($errors->any())
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
	@endif
	<div class="row">
		<div class="col-sm-5 productimage">
			<img src="/{{ $product_review->featured_image1 }}"/>
		</div>
		<div class="col-sm-7">
			<h2>{{ $product_review->name }}</h2>
			<p>
				@if(isset($product_review->available_quantity))
				Quantity: {{ $product_review->available_quantity}}
				@else
				Quantity: {{ "123" }}
				@endif
			</p>
			<p>{{ $product_review->short_intro }}</p>
			<span class="price_pro">£ {{ $product_review->price }}</span>
		</div>
	</div>
	<hr>
	<div class="row miscellaneous_info">

		<div class="col-sm-6">
			<p>Product Id: #{{$product_review->id}}</p>
			@if(!isset($product_review->in_out_stock))
			<span class="in_stock"><i class="fa fa-check"></i> In Stock</span>
			@else
			<span class="in_stock"><i class="fa fa-close"></i> Out of Stock</span>
			@endif
			<p class="note">Please note images are for illustration purposes and may differ from the product(s) you receive</p>
		</div>
		<div class="clear"></div>
		<div class="col-sm-6">
			<p>@if(isset($product_review->meta_description))
				{{ $product_review->meta_description }}
				@else
				{{ "Not Available, So this is the junk text to occupy the space in the site" }}
				@endif
			</p>
		</div>
	</div>
	<div class="row review_form">
		<div class="tab">
			<button class="tablinks active" onclick="openTab(event, 'description');">Product Description</button>
			<button class="tablinks " onclick="openTab(event, 'useage');">Usage / Instruction</button>
			<button class="tablinks " onclick="openTab(event, 'warnings');">Warnings</button>
			<button class="tablinks " onclick="openTab(event, 'ingredients');">Ingredients</button>
			<button class="tablinks " onclick="openTab(event, 'review');">Reviews &amp; Ratings</button>
		</div>
		<div class="tabcontent" id="description" style="display:block;">
			<p><strong>{{$product_review->name}}</strong><br>
				{{ $product_review->description }}
			</p>
		</div>
		<div class="tabcontent" id="useage" style="display: none;">
			<p><strong>Use:</strong> {{$product_review->useage}}</p>
			<p><strong>Directions for adults and children 12 years and over</strong></p>
			<ul>
				<li>Lorem ipsum donor si tenet</li>
				<li>Lorem ipsum donor si tenet</li>
				<li>Lorem ipsum donor si tenet</li>
			</ul>
			<p><strong>Using technique</strong></p>
			<ul>
				<li>Lorem ipsum donor si tenet</li>
				<li>Lorem ipsum donor si tenet</li>
				<li>Lorem ipsum donor si tenet</li>
			</ul>
		</div>
		<div class="tabcontent" id="warnings" style="display: none;">
			<p><strong>Warning:</strong> Do not take more medicine than the label tells you to.<br>
				If you are pregnant, or you need any other advice before using this product, talk to your doctor, pharmacist or nurse.<br>
				Do not use if you are allergic to any of the ingredients listed in the enclosed leaflet.<br>
			Keep out of the sight and reach of children.</p>
		</div>
		<div class="tabcontent" id="ingredients" style="display: none;">
			<p>
				@if($product_review->ingredients)
				{{$product_review->ingredients}}
				@else
				{{ "Includes: Chewing Gum Base, Xylitol, Acesulfame Potassium, Polacrilin, Sodium Carbonate, Sodium Bicarbonate, Peppermint Oil, Menthol, Magnesium Oxide, E171, E321, Talc, Acacia and Carnauba Wax" }}
				@endif
			</p>
		</div>
		<div class="tabcontent" id="review" style="display: none;">
			<div id="reviewCarousel" class="carousel slide" data-ride="carousel">
				<ol class="carousel-indicators">
					@foreach($reviews as $review)
					<li data-target="#reviewCarousel" data-slide-to="{{ $loop->index }}" class="{{ $loop->first ? 'active' : '' }}"></li>
					@endforeach
				</ol>
				<div class="carousel-inner">
					@foreach($reviews as $review)
					<div class="customer-rating carousel-item {{ $loop->first ? ' active' : '' }}">
						<p><strong>{{ $review->name }}</strong></p>
						<?php
						$count = $review->overall;
						for($i = 0; $i < $count; $i++){ ?>
							<i class="fa fa-star rating selected" aria-hidden="true"></i>
						<?php } ?>
						<p>{{ $review->reviewBox }}</p>
					</div>
					@endforeach
				</div>

				<a class="carousel-control-prev" href="#reviewCarousel" role="button" data-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="carousel-control-next" href="#reviewCarousel" role="button" data-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>

			<p class="write_review" onclick="addReview();">
				Write a Review
			</p>
			<form method="POST" action="/product-detail/{{ $product_review->id }}/review" enctype="multipart/form-data" id="reviewForm" style="display: none;">
				@csrf
				<h4>Add Your Review</h4>
				<div class="form-group">
					<label for="value_money">Value for Money</label>
					<div id="rating-stars">
						<i class="fa fa-star rating " aria-hidden="true" onclick="turnSelected('rating-stars',1);"></i>
						<i class="fa fa-star rating " aria-hidden="true" onclick="turnSelected('rating-stars',2);"></i>
						<i class="fa fa-star rating " aria-hidden="true" onclick="turnSelected('rating-stars',3);"></i>
						<i class="fa fa-star rating " aria-hidden="true" onclick="turnSelected('rating-stars',4);"></i>
						<i class="fa fa-star rating " aria-hidden="true" onclick="turnSelected('rating-stars',5);"></i>
						<input type="text" readonly name="value_money" id="value_money" value="0" class="form-control"/>
					</div>
				</div>
				<div class="form-group">
					<label for="quality">Quality</label>
					<div id="quality-stars">
						<i class="fa fa-star rating " aria-hidden="true" onclick="turnSelected('quality-stars',1);"></i>
						<i class="fa fa-star rating " aria-hidden="true" onclick="turnSelected('quality-stars',2);"></i>
						<i class="fa fa-star rating " aria-hidden="true" onclick="turnSelected('quality-stars',3);"></i>
						<i class="fa fa-star rating " aria-hidden="true" onclick="turnSelected('quality-stars',4);"></i>
						<i class="fa fa-star rating " aria-hidden="true" onclick="turnSelected('quality-stars',5);"></i>
						<input type="text" readonly name="quality" id="quality" value="0"  class="form-control"/>
					</div>
				</div>
				<div class="form-group">
					<label for="performance">Performance</label>
					<div id="perfor-stars">
						<i class="fa fa-star rating " aria-hidden="true" onclick="turnSelected('perfor-stars',1);"></i>
						<i class="fa fa-star rating " aria-hidden="true" onclick="turnSelected('perfor-stars',2);"></i>
						<i class="fa fa-star rating " aria-hidden="true" onclick="turnSelected('perfor-stars',3);"></i>
						<i class="fa fa-star rating " aria-hidden="true" onclick="turnSelected('perfor-stars',4);"></i>
						<i class="fa fa-star rating " aria-hidden="true" onclick="turnSelected('perfor-stars',5);"></i>
						<input type="text" readonly name="performance" id="performance" value="0"  class="form-control"/>
					</div>
				</div>
				<div class="form-group show_overallRating">
					<label for="overall">Overall Rating</label>
					<input type="hidden" class="form-control" readonly name="overall" id="overallRating" value="0"/>
					<span id="overall_rating">0/5</span>
				</div>
				<h4><strong>Share Your Experience (optional)</strong></h4>
				@if(!Auth::check())
				<div class="row other_details">
					<div class="col-sm-6 form-group">
						<label for="name">Display Name</label><br>
						<input type="text" class="form-control" name="name" required/>
					</div>
					<div class="col-sm-6 form-group">
						<label for="email">Email</label><br>
						<input type="text" class="form-control" name="email" required/>
					</div>
				</div>
				@else
				<input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
				<div class="row other_details">
					<div class="col-sm-6 form-group">
						<label for="name">Display Name</label><br>
						<input type="text" class="form-control" name="name" required value="{{ Auth::user()->name }}"/>
					</div>
					<div class="col-sm-6 form-group">
						<label for="email">Email</label><br>
						<input type="text" class="form-control" value="{{ Auth::user()->email }}" name="email" required/>
					</div>
				</div>
				@endif
				<div class="form-group">
					<label for="recommend">Would you recommend this product to others?</label><br>
					<input type="radio" name="recommend" value="Yes"/>Yes&nbsp;&nbsp;&nbsp;
					<input type="radio" name="recommend" value="No"/>No
				</div>
				<div class="form-group">
					<label for="reviewBox">Type your product review in the space provided</label>
					<textarea name="reviewBox" class="form-control"></textarea>
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-md submit_btn">Submit Review</button>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">

	function turnSelected(id, value){
		var i;
		var stars;
		var rateOne, rateTwo, rateThree, overallRate;
		stars = document.getElementById(id).children;
		for(i = 0; i < 5; i++){
			if(stars[i].classList.contains("selected"))
			{
				stars[i].classList.remove("selected");
			}
		}
		for (i = 0; i < value; i++){	
			stars[i].classList.add("selected");
		}
		stars[5].value = value ;
		rateOne = parseFloat(document.getElementById('value_money').value);
		rateTwo = parseFloat(document.getElementById('quality').value);
		rateThree = parseFloat(document.getElementById('performance').value);
		overallRate = (rateOne + rateTwo + rateThree)/3;
		overallRate = Math.round(overallRate * 10)/ 10;
		overallRate = overallRate;
		document.getElementById('overall_rating').innerHTML = overallRate + '/5';
		document.getElementById('overallRating').value = overallRate;
	}


	function openTab(evt, tabName){
		var tabcontent = document.getElementsByClassName('tabcontent');
		var tablinks = document.getElementsByClassName('tablinks');
		var i;
		for(i = 0; i < tablinks.length; i++){
			tablinks[i].classList.remove("active");
		}
		for(i = 0; i < tabcontent.length; i++){
			tabcontent[i].style.display = "none";
		}
		evt.currentTarget.classList.add("active");
		document.getElementById(tabName).style.display = "block";
	}
	function addReview(){
		document.getElementById("reviewForm").style.display = "block";
	}
</script>
@endsection