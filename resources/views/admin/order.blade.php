@extends('layouts.admin')

@section('content')
<div class="container-fluid">
	@if (session()->has('success_message'))
	    <div class="alert alert-success">
	        {{ session()->get('success_message') }}
	    </div>
	@endif
	
	@if($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach($errors->all() as $error)
					<li>{{$error}}</li>
				@endforeach
			</ul>
		</div>
@endif
<div class="row">
	<div class="col-sm-3 customer_sidebar">
		@include('layouts.sidebar')
	</div>
	<div class="col-sm-9 customer_playzone center">
		<orders :orders="{{ json_encode($orders) }}"></orders>
		<a href="/exportCSV" class="btn btn-info">Export as CSV</a>
	</div>
</div>
</div>
@endsection
