<html>
<head>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <style type="text/css">
        body{
            font-family: 'Roboto', sans-serif;
            padding: 0;
            margin: 0;
        }
        #wrapper{
            width: 70%;
            max-width: 600px;
            margin: 0 auto;
            box-shadow: 0px 3px 6px #000;
            border: 1px solid #ccc;
        }

        #header{

            width: 100%;
            height: 155px;
            padding: 20px 0 7px;
        }

        #header h4 {
            color: #000000;
            font-size: 30px;
            font-weight: 500;
            text-transform: uppercase;
        }


        #content h5 {
            color: #666666;
            font-size: 24px;
            font-weight: 700;
        }

        #content h6 {
            color: #04a7e0;
            font-size: 30px;
            font-weight: 700;
        }

        #content p {
            color: #666666;
            font-family: Roboto;
            font-size: 16px;
            font-weight: 400;
            margin-bottom: 70px;
        }

        #footeremail a{
            color:#fff;
        }

        #footer{
            background: #2170b5;
            height: 150px;
            width: 100%;
            padding: 20px 0 22px;
        }

        #footer p{
            color: #fff;
            font-size: 16px;
            font-weight: 400;
            line-height: 24px;
        }

        .text-center{
            text-align: center;
        }

        .top_30{
            margin-top: 30px;
        }

        #footer_social_icons li{
            display: inline-block;
            margin: 0px 3px;
            list-style: none;
        }
    </style>
</head>
<body>

<div id="wrapper" style="width:70%;margin:0 auto;"  class="text-center">
    <div id="header" style="background:#2170b5;">

        <img src="http://pharmacysaver.lucentbusiness.com/images/homepage-images/nutritionplanet.jpg" style="
    outline: none;
    text-decoration: none;
    max-width: 100%;
    clear: both;
    display: block;
    margin: 0 auto;
    width: 282px;
    padding-bottom: 13px;
    padding-top: 10px;">
    </div><!-- #header -->

    <div id="content">
        <h2 style="text-align:center;color: #000000;font-family: Roboto;font-size: 24px;font-weight: 400;margin-top: 50px;margin-bottom: 0px;">Order Received of ID {{$order_id}}</h2>
        <h2 class="attention" style="color: inherit;font-family: Helvetica, Arial, sans-serif;font-weight: 400;text-align: center;line-height: 1.3;word-wrap: normal;font-size: 14px;margin: 27px 0 10px;padding: 0;" align="center">Attention Nutrition Planet<b></b></h2>
        <p class="email-content" style="color: #666666;font-family: Helvetica, Arial, sans-serif;font-weight: normal;text-align: center;line-height: 1.3;font-size: 14px;margin: 0 0 10px;padding: 17px 43px;margin-bottom: 27px;" align="center">
            The Following order of  {{$order_id}} has been made </p>

        <table style="width:70%;margin:0 auto;">
            <tr>
                <td>
                    <b>Order ID</b>
                </td>
                <td>
                    {{$order_id}}
                </td>
            </tr>
            <tr>
                <td>

                    <b> Products </b>
                </td>
                <td>
                    <table>
                        @foreach($order_products as $order_product)
                        <tr>
                            <td>
                                {{$order_product->product->name}}
                            </td>
                        </tr>
                            @endforeach
                    </table>

                </td>
            </tr>
            <tr>
                <td>
                    <b>Total Amount Paid</b>
                </td>
                <td>
                    £ {{$orderDetails->billing_total}}
                </td>
            </tr>

            <tr>
                <td>
                    <b>Customer Phone Number</b>
                </td>
                <td>
                    {{$orderDetails->delivery_phone}}
                </td>
            </tr>
            <tr>
                <td>
                    <b>Customer Address</b>
                </td>
                <td>
                    {{$orderDetails->delivery_address}}   {{$orderDetails->delivery_city}}   {{$orderDetails->delivery_province}}  {{$orderDetails->delivery_postalcode}}
                </td>
            </tr>

            <tr>
                <td>
                    <b>Customer Email</b>
                </td>
                <td>
                    {{$orderDetails->delivery_email}}
                </td>
            </tr>
        </table>


    </div><!-- #content -->
    <div id="footer" style="
    background: #2170b5;
    height: 150px;
    width: 100%;
    padding: 20px 0 22px;
    text-align: center;
    color: #fff;
    margin-top: 32px;">

        <img src="http://pharmacysaver.lucentbusiness.com/images/homepage-images/nutritionplanet.jpg" alt="" width="" style="    width: 40%;
    margin: 0 auto;"/>

         <p>Devonshire House 582 Honeypot Lane Stanmore Middlesex HA7 1JS <br />




            <span id="footeremail" style="color:#fff;"><a style="color:#fff;">enquiry@pharmacysaver.co.uk</a></span> | 020 8732 5465</p>

    </div><!-- #footer -->


</div><!-- #wrapper -->
</body>
</html>
    