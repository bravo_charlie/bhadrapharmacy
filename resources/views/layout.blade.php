<!DOCTYPE html>
<html>
<head>
<title></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
<!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<meta name="csrf-token" content="{{ csrf_token() }}">

<link href="https://fonts.googleapis.com/css?family=Josefin+Sans" rel="stylesheet">
  <link href='https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900|Material+Icons' rel="stylesheet">

<link href="/css/style.css" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('css/app.css') }}">
<link rel="stylesheet" href="{{ asset('css/responsivelayout.css') }}">

 <link rel="stylesheet" href="{{asset('css/responsive.css')}}">

<script type="text/javascript">
	function openTab(evt, tabname){
		var i, tablinks, tabcontent;
		tabcontent = document.getElementsByClassName('tabcontent');
		for(i = 0; i < tabcontent.length; i++){
			tabcontent[i].style.display = "none";
		}
		tablinks = document.getElementsByClassName('tablinks');
		for(i = 0; i < tablinks.length; i++){
			tablinks[i].className = tablinks[i].className.replace("active", "");
		}
		document.getElementById(tabname).style.display = "block";
		evt.currentTarget.className += "active";
	}
	function closeTab(id) {
		document.getElementById(id).style.display="none";
	}
	function closeAll(){
		var tabClass = document.querySelectorAll('.tabcontent');
		tabClass.forEach(element => {
		  element.style.display = 'none';
		});
	}
</script>
 
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
@yield('extra-css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet"/> 
</head>
<body>
	<div id='app'>
		<?php 
			use App\Menu;
			$main_menu = Menu::all();

			use App\SubMenu;
			$sub_menu = SubMenu::orderBy('mainmenuid', 'ASC')->orderBy('order', 'ASC')->get();

			use App\ProductCategory;
			$categories = ProductCategory::orderBy('selectmainmenu', 'ASC')->orderBy('selectsubmenu', 'ASC')->orderBy('order', 'ASC')->get();
			
		?>

		@include('layout.header')

				
			
			@include('layouts.success_message')
					
			@yield('content')
			
			<div class="cookie-popup" id="cookie-policy" style="display: none;">
				<div class="container">
					<div class="row d-flex align-items-center">
						<div class="col-sm-8">
							<h4 class="font-weight-bold">Your Privacy Matters</h4>
							<p>
								We use cookies to analyse site usage, provide social media features and personalise content and ads. We may also share information about your use of our website with our partners. <a href="/privacy-policy">View our privacy policy</a> &nbsp;/  <a href="/terms-of-use">View our Cookies policy</a>
							</p>
						</div>
						<div class="col-sm-4 text-center">
							<button id="cookie-close" class="btn btn-primary btn-lg">Close</button>
						</div>
					</div>
				</div>
			</div>
		@include('layouts.footer')
	</div>
	<script>
		$(document).ready(function(){
		  $('.show-sub-menu').on("click", function(e) {
		    $(this).parent().siblings().find('ul').hide();
		    $(this).next('ul').toggle();
		    e.stopPropagation();
		    e.preventDefault();
		  });

		  function getCookie(name) {
		      var v = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
		      return v ? v[2] : null;
		  }

		  if(getCookie('np-privacy-cookie') == null) {
		  	$('#cookie-policy').show();
		  }

		  $('#cookie-close').click(function() {
		  	document.cookie = "np-privacy-cookie=true; expires=Thu, 18 Dec 2022 12:00:00 UTC";
		  	$('#cookie-policy').hide();
		  });

		  var box = document.querySelector("#mobile_Menu");
		  document.addEventListener("click", function(event) {
		    if (event.target.closest(".dropdown-menu")) 
		      return;
		    else{
		      $(".mobile_sub_menu").css("display","none");
		      $(".collapse.show").removeClass("show");
		    }
		  });

		});

		
	</script>
	
	{!! Html::script(mix('js/app.js')) !!}
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.full.js"></script>
	@yield('extra-js')
</body>
</html>