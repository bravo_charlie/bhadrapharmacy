@extends('layouts.admin')

@section('content')
<div class="container-fluid">
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
	<div class="row">
		<div class="col-sm-3 admin_sidebar">
			@include('layouts.sidebar')
		</div>
		<div class="col-sm-9 col-sm-8">
			<form method="post" action="/menu/edit/{{$menu->id}}" enctype="multipart/form-data">
			@csrf
				<h3>Edit New Menu</h3>
				<div class="row">
					<div class="col-sm-2 picture-container dealer_setting_img">
					    <div class="card_img_box picture top_10">
					    	@if( !empty($menu->featured_image) )
					    		<img src="/{{ $menu->featured_image }}" class="dealer_logo_url">
					    	@else
					        <img src="{{ url('images/icons/card_upload.svg') }}" class="dealer_logo_url">
					        @endif
					        <input type="file" name="featured_image" class="card_img_file">
					        <h6>Upload Photo</h6>
					    </div>
					</div>
					<div class="col-sm-10">
						<div class="form-group">
							<input type="hidden" value="{{csrf_token()}}" name="_menuName"/>
							<label for="menuName">Menu Name:</label>
							<input class="form-control" type="text" name="menuName" value='{{ $menu->menuName }}'/>
						</div>
						<div class="form-group">
							<label for="order">Menu Order:</label>
							<input class="form-control" type="number" name="order" value="{{ $menu->order}}"/>
						</div>
						<div class="form-group">
							<button type="submit" class="form-control btn btn-primary btn-block">Submit</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection