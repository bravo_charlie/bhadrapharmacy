@extends('layout')


@section('content')
<section>
	<div class="crumb">  
		<ul class="mobile-hidden" >
			<li >
				<a href="/">
					<span itemprop="name">Home</span> </a> 
				</li> 
				<li>
					<a href="/main-menu/{{$sub_menu->menu->id}}">

						<span >{{ $sub_menu->menu->menuName }}</span> </a>  

					</li>
					<li>
						<a href="/sub-menu/{{$sub_menu->id}}">
							<span >{{ $sub_menu->subMenuName }}</span> </a>  
						</li>
					</ul>
				</div>
				<div class="page-title-block"> <span class="page-title-line">
					<h1 class="page-title"> <span> {{$sub_menu->subMenuName}} </span> </h1>
				</span> </div>
				<div class="container">
					<div class="sub-category-block">
						<ul class="sub-category-links">
							@foreach( $sub_menu->productCategory as $category)
							<li>
								<a href="/products/{{$category->id}}">{{ $category->name}}</a>
							</li>
							@endforeach
						</ul>
					</div>
				</div>
			</section>

			<section class="homepage-slider">
				<div class="container">		
					<div class="row">
						<div class="col-sm-2">
							<div class="filter-block p-1">
								<h4>Price</h4>
								<ul>
									@foreach($priceFilterArray as $pFilter)
									<li>
										<input class="price-filter" type="checkbox" name="filter-price" value="{{ $pFilter['url'] }}"
										 {{ ($pFilter['slug'] == $priceFilter)?'checked':'' }} >
										<label>{{ $pFilter['title'] }}</label>
									</li>
									@endforeach
								</ul>
							</div>
						</div>
						<div class="col-sm-10">
							<div class="menu-product animated fadeInUp animatedfadeInUp">
								<div class="row">
									<div class="col-sm-12">
										<div class="pb-3">
											<h5 class="d-inline">Sort By:</h4>
											<select name="sort" class="sort-select">
												<option value="">Select Order By</option>
												@foreach($sortArray as $sort)
												<option value="{{$sort['url']}}" {{ ($sort['slug']==$order)?'selected':'' }}>{{ $sort['title'] }}</option>
												@endforeach
											</select>
										</div>
									</div>
								</div>
								<div class="row">
									@foreach($products as $product)
									<div class="col-sm-3 zoom-effect">
										<header>
											<a href="/product-detail/{{$product->id}}"><img src="/{{$product->featured_image1}}" class="cat-img"/></a>
										</header>
										<div class="main-product">
											<a href="/product-detail/{{$product->id}}">
												<h4 class="cat-name">
													{{$product->name}}
													<p>
														@if(empty($product->discount))
															£ {{ $product->price }}
														@else
															£ {{ $product->price-$product->discount }} <strike class="price-strike">£ {{ $product->price }}</strike>
														@endif
													</p>
												</h4>
											</a>
										</div>
									</div>
									@endforeach
								</div>
									<!-- {{ $products->render() }} -->
									 {{ $products->appends(request()->input())->links() }}
							</div>
						</div><!-- .col-sm-9 -->
					</div>
				</div>
			</section>

			<div class="container">
<!-- 				<p class="note-para">*The Best Food that you can find in your hometowm. Come and make each of your moments especial and memorable</p>
 -->			</div>

			

			@endsection
@section('extra-js')
   <script type="text/javascript">
				$(document).ready(function() {
					$('.price-filter').click(function() {
						var url = window.location.href.split('?')[0];
						if(this.checked) {
							location.href = url+this.value;
						} else {
							location.href = url;
						}

					});

					$('.sort-select').change(function() {
						$this = $(this);
						var url = window.location.href.split('?')[0];
						location.href = url+$this.val();
					});
				});
			</script>
    
@endsection