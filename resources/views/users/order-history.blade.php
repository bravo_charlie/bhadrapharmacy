@extends('layouts.admin')

@section('content')
<div class="container-fluid">
@if($errors->any())
	<div class="alert alert-danger">
		<ul>
			@foreach($errors->all() as $error)
				<li>{{$error}}</li>
			@endforeach
		</ul>
	</div>
@endif
<div class="row">
	<div class="col-sm-3 customer_sidebar">
		@include('layouts.sidebar')
	</div>
	<div class="col-sm-9 customer_playzone center">
		<!-- <h3>Order History</h3> -->
		<!-- <span>Click the Order Number to reorder</span> -->
		<order-history :orders="{{ json_encode($orders) }}"></order-history>
		<a href="/exportCSV" class="btn btn-info">Export as CSV</a>
		<!-- <table class="table table-hover">
			<thead>
				<td>Order Id</td>
				<td>Products</td>
				<td>Date of Order</td>
				<td>Status</td>
				<td>Delivery Date</td>
				<td>Remarks</td>
			</thead>
			<hr class="hr-separator">
			<tbody>
				<td><a href="#">12345</a></td>
				<td>ABCD</td>
				<td>12th Dec</td>
				<td>Processing</td>
				<td>20th Dec</td>
				<td>Needed as soon as possible!</td>
			</tbody>
		</table> -->
	</div>
</div>
</div>
@endsection
