@extends('layouts.admin')

@section('content')
<div class="container-fluid">
	@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
	<div class="row">
		
		<div class="col-sm-9 customer_playzone customer_profile center">	
			<i class="fa fa-user-o" aria-hidden="true"></i>
			<h3>{{ $currentUser->name }}</h3>
			<i>Id: <span>{{ $currentUser->id}}</span></i><br><br>
			<table class="table table-hover">
				<thead>
					<td colspan="2">User Data</td>
				</thead>
				<tbody>
					<td>Name:</td>
					<td>{{ $currentUser->name }}</td>
				</tbody>
				<tbody>
					<td>Email:</td>
					<td>{{ $currentUser->email }}</td>
				</tbody>
				<tbody>
					<td>Password:</td>
					<td><a href="/changePassword">Change password</a></td>
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection