@extends('layouts.admin')

@section('content')
	<div class="container slider-index">
	<div class="card">
		<div class="row">
        	<div class="col-sm-12 admin_playzone">
        		<coupons :coupons="{{ json_encode($coupons) }}"></coupons>
        	</div>
		</div>
		</div>
	</div>
@endsection