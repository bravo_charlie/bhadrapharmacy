@extends('layout')
@section('content')
<div class="container">
	<div class="row">
		<div class="col">
			@if (session()->has('success_message'))
                <div class="alert alert-success">
                    {{ session()->get('success_message') }}
                </div>
            @endif

            @if(count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
		</div>
		
	</div>
	<div class="row">
		<div class="col">
			<p>
				<a href="/product-cat-list" class="button">Continue Shopping</a>
			</p>
		</div>
	</div>
	
</div>
@endsection
