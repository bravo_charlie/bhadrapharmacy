@extends('layouts.admin')
@section('content')
<div class="container product-show-user">
<div class="row">
<div class="col-sm-6">
<div id="productCarousel" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#productCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#productCarousel" data-slide-to="1"></li>
    <li data-target="#productCarousel" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block" src="https://image.made-in-china.com/43f34j00oevTHgLRYBrj/Veterinary-Drug-Ciprofloxacin-Lactate-Soluble-Powder.jpg" alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block" src="https://image.made-in-china.com/43f34j00NveaLDPsfBrS/Veterinary-Medicine-Tylosin-Phosphate-Premix.jpg" alt="Second slide">
    </div>
    <div class="carousel-item">
      <img class="d-block" src="https://thesocialmedwork.com/media/catalog/product/cache/aefcd4d8d5c59ba860378cf3cd2e94da/e/m/empliciti-elotuzumab.jpg" alt="Third slide">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
<div class="col-sm-6">
<div class="product_desc_detail">
{!! Form::open(['url' => '', 'method' => 'POST' ]) !!}
@csrf
<div class="form-group">

</div>
{!! Form::close() !!}
</form>
</div>
</div>
</div>
</div>
@endsection