@extends('layout')


@section('content')

<div class="crumb">  
	<ul class="mobile-hidden" >
		<li >
			<a href="/">
				<span itemprop="name">Home</span> </a> 
			</li> 
			<li>
				<a href="/main-menu/{{$main_menu->id}}">

					<span >{{$main_menu->menuName}}</span> </a>  

				</li>
			</ul>
		</div>
		<div class="page-title-block"> <span class="page-title-line">
			<h1 class="page-title"> <span> {{$main_menu->menuName}} </span> </h1>
		</span> </div>
		<div class="container">
			<div class="sub-category-block">
				<ul class="sub-category-links">
					@foreach( $main_menu->subMenu as $sub )
					<li>
						<a href="/sub-menu/{{$sub->id}}">{{ $sub->subMenuName}}</a>
					</li>
					@endforeach
				</ul>
			</div>
		</div>

		<section class="homepage-slider">
			<div class="container">		
				<div class="row">
					<div class="menu-product animated fadeInUp animatedfadeInUp">
						<div class="row">
							@foreach($products as $product)
							<div class="col-sm-3 zoom-effect">
								<header>
									<a href="/product-detail/{{$product->id}}"><img src="/{{$product->featured_image1}}" class="cat-img"/></a>
								</header>
								<div class="main-product">
									<a href="/product-detail/{{$product->id}}">
										<h4 class="cat-name">
											{{$product->name}}
											<p>
												@if(empty($product->discount))
													£ {{ $product->price }}
												@else
													£ {{ $product->price-$product->discount }} <strike class="price-strike">£ {{ $product->price }}</strike>
												@endif
											</p>
										</h4>
									</a>
								</div>
							</div>
							@endforeach
						</div>
						{{ $products->render() }}
					</div>
				</div>
			</div>
		</section>
		<div class="container">
			<!-- <p class="note-para">Come and make each of your moments especial and memorable</p> -->
		</div>

		@endsection