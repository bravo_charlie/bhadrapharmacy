@extends('layout')

@section('content')
<div class="container  animated fadeInUp animatedfadeInUp">
	<h2 class="review">
			Our Reviews
		</h2>
		<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.</p>
		<p>Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus.</p>
	<div class="row net-review">
			@foreach($reviews as $review)					
			<div class="col-sm-4 customer-rating">
				<p><strong>{{ $review->name }}</strong></p>
				<?php
				$count = $review->overall;
				for($i = 0; $i < $count; $i++){ ?>
					<i class="fa fa-star rating selected" aria-hidden="true"></i>
					<?php } ?>
				<p>{{ $review->reviewBox }}</p>
			</div>
			@endforeach	
	</div>
</div>
@endsection