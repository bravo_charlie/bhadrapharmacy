@extends('layout')
@section('content')
<div class="container privacy-policy  animated fadeInUp animatedfadeInUp">
	<h2>Privacy Policy</h2>
	<h5>Introduction</h5>
	<p>This privacy policy applies between you, the User of this Website and Curamed Limited, the owner
and provider of this Website. Curamed Limited takes the privacy of your information very seriously.
This privacy policy applies to our use of any and all Data collected by us or provided by you in
relation to your use of the Website.
	</p>
	<p>Please read this privacy policy carefully.</p>

	<ol class="deep-list">
		<li>Definitions and interpretation
			<ol class="first-child">
				<li>In this privacy policy, the following definitions are used:</li>
				<table class="table">
  
  <tbody>
    <tr>
     
      <td>Data  </td>
      <td>collectively all information that you submit to
Curamed Limited via the Website. This definition
incorporates, where applicable, the definitions
provided in the Data Protection Laws;</td>
     
    </tr>
    
    <tr>
           <td>Cookies</td>
      <td>a small text file placed on your computer by this
Website when you visit certain parts of the Website
and/or when you use certain features of the Website.
Details of the cookies used by this Website are set
out in the clause below (Cookies);</td>
          </tr>
          <tr>
           <td>Data Protection Laws</td>
      <td>any applicable law relating to the processing of
personal Data, including but not limited to the
Directive 96/46/EC (Data Protection Directive) or the
GDPR, and any national implementing laws,
regulations and secondary legislation, for as long as
the GDPR is effective in the UK;</td>
          </tr>
          <tr>
           <td>GDPR</td>
      <td>the General Data Protection Regulation (EU)
2016/679;</td>
          </tr>
          <tr>
           <td>Curamed Limited, or us</td>
      <td>Curamed Limited, a company incorporated in
England and Wales with registered number 05142946
whose registered office is at Devonshire House, 582
Honeypot Lane, Stanmore, Middlesex, HA7 1JS;</td>
          </tr>
          <tr>
           <td>UK and EU Cookie Law</td>
      <td>the Privacy and Electronic Communications (EC
Directive) Regulations 2003 as amended by the
Privacy and Electronic Communications (EC
Directive) (Amendment) Regulations 2011;</td>
          </tr>
          <tr>
           <td>User or you </td>
      <td>any third party that accesses the Website and is not
either (i) employed by Curamed Limited and acting
in the course of their employment or (ii) engaged as a
consultant or otherwise providing services to
Curamed Limited and accessing the Website in
connection with the provision of such services; and</td>
          </tr>
          <tr>
           <td>Website </td>
      <td>the website that you are currently using,
www.nutritionplanet.co.uk, and any sub-domains of
this site unless expressly excluded by their own terms
and conditions.</td>
          </tr>
  </tbody>
</table>
<li>In this privacy policy, unless the context requires a different interpretation:<br>
					a. the singular includes the plural and vice versa;<br>
b. references to sub-clauses, clauses, schedules or appendices are to sub-clauses, clauses,
schedules or appendices of this privacy policy;<br>
c. a reference to a person includes firms, companies, government entities, trusts and partnerships;<br>
d. &quot;including&quot; is understood to mean &quot;including without limitation&quot;;<br>
e. reference to any statutory provision includes any modification or amendment of it;<br>
f. the headings and sub-headings do not form part of this privacy policy.
				</li>
				
				
				
				<h4>Scope of this privacy policy</h4>
				<li>
					<p>This privacy policy applies only to the actions of Curamed Limited and Users with respect to this
Website. It does not extend to any websites that can be accessed from this Website including, but
not limited to, any links we may provide to social media websites.</p></li><li>
					<P>For purposes of the applicable Data Protection Laws, Curamed Limited is the &quot;data controller&quot;.
This means that Curamed Limited determines the purposes for which, and the manner in which,
your Data is processed./P>
			</li>
			
			<h4>Data collected</h4>
				<li>We may collect the following Data, which includes personal Data, from you:
				a. name;<br>
b. date of birth;<br>
c. contact Information such as email addresses and telephone numbers;<br>
d. demographic information such as postcode, preferences and interests;<br>
e. financial information such as credit / debit card numbers;<br>
f. IP address (automatically collected);<br>
g. in each case, in accordance with this privacy policy.	
			</li>
			
			
			<h4>How we collect Data</h4>
				<li>We collect Data in the following ways:<br>
				a. data is given to us by you; and<br>
b. data is collected automatically.	
			</li>
			
			<h4>Data that is given to us by you</h4>
				<li>Curamed Limited will collect your Data in a number of ways, for example:
				a. when you contact us through the Website, by telephone, post, e-mail or through any other
means;<br>
b. when you register with us and set up an account to receive our products/services;<br>
c. when you make payments to us, through this Website or otherwise;<br>
d. when you use our services;<br>
in each case, in accordance with this privacy policy.	
			</li>
			
			<h4>Data that is collected automatically</h4>
				<li>To the extent that you access the Website, we will collect your Data automatically, for example:<br>
				a. we automatically collect some information about your visit to the Website. This information
helps us to make improvements to Website content and navigation, and includes your IP
address, the date, times and frequency with which you access the Website and the way you use
and interact with its content.<br>
b. we will collect your Data automatically via cookies, in line with the cookie settings on your
browser. For more information about cookies, and how we use them on the Website, see the
section below, headed &quot;Cookies&quot;.
			</li>
			
			<h4>Our use of Data</h4>
				<li>Any or all of the above Data may be required by us from time to time in order to provide you with
the best possible service and experience when using our Website. Specifically, Data may be used
by us for the following reasons:<br>
				a. internal record keeping;<br>
b. transmission by email of marketing materials that may be of interest to you;<br>
c. contact for market research purposes which may be done using email, telephone, fax or mail.
Such information may be used to customise or update the Website;<br>
in each case, in accordance with this privacy policy.
			</li>
			<li>We may use your Data for the above purposes if we deem it necessary to do so for our legitimate
interests. If you are not satisfied with this, you have the right to object in certain circumstances
(see the section headed &quot;Your rights&quot; below).
			</li>
			<li>For the delivery of direct marketing to you via e-mail, we&#39;ll need your consent, whether via an
opt-in or soft-opt-in:<br>
a. soft opt-in consent is a specific type of consent which applies when you have previously
engaged with us (for example, you contact us to ask us for more details about a particular
product/service, and we are marketing similar products/services). Under &quot;soft opt-in&quot; consent,
we will take your consent as given unless you opt-out.<br>
b. for other types of e-marketing, we are required to obtain your explicit consent; that is, you need
to take positive and affirmative action when consenting by, for example, checking a tick box
that we&#39;ll provide.<br>
c. if you are not satisfied about our approach to marketing, you have the right to withdraw consent
at any time. To find out how to withdraw your consent, see the section headed &quot;Your rights&quot;
below.
			</li>
			<li>When you register with us and set up an account to receive our services, the legal basis for this
processing is the performance of a contract between you and us and/or taking steps, at your
request, to enter into such a contract.
			</li>
			
			<h4>Keeping Data secure</h4>
			<li>We will use technical and organisational measures to safeguard your Data, for example:<br>
			a. access to your account is controlled by a password and a user name that is unique to you.<br>
b. we store your Data on secure servers.<br>
c. payment details are encrypted using SSL technology (typically you will see a lock icon or green
address bar (or both) in your browser when we use this technology.
			</li>
			<li>Technical and organisational measures include measures to deal with any suspected data breach.
If you suspect any misuse or loss or unauthorised access to your Data, please let us know
immediately by contacting us via this e-mail address: info@nutritionplanet.co.uk.
			</li>
			<li>If you want detailed information from Get Safe Online on how to protect your information and
your computers and devices against fraud, identity theft, viruses and many other online problems,
please visit www.getsafeonline.org. Get Safe Online is supported by HM Government and leading
businesses.
			</li>
			
			<h4>Data retention</h4>
			<li>Unless a longer retention period is required or permitted by law, we will only hold your Data on
our systems for the period necessary to fulfil the purposes outlined in this privacy policy or until
you request that the Data be deleted.
			</li>
			li>Even if we delete your Data, it may persist on backup or archival media for legal, tax or
regulatory purposes.
			</li>
			
			<h4>Your rights</h4>
			<li>You have the following rights in relation to your Data:<br>
			a. Right to access - the right to request (i) copies of the information we hold about you at any
time, or (ii) that we modify, update or delete such information. If we provide you with access to
the information we hold about you, we will not charge you for this, unless your request is
&quot;manifestly unfounded or excessive.&quot; Where we are legally permitted to do so, we may refuse
your request. If we refuse your request, we will tell you the reasons why.<br>
b. Right to correct - the right to have your Data rectified if it is inaccurate or incomplete.<br>
c. Right to erase - the right to request that we delete or remove your Data from our systems.<br>
d. Right to restrict our use of your Data - the right to &quot;block&quot; us from using your Data or limit
the way in which we can use it.<br>
e. Right to data portability - the right to request that we move, copy or transfer your Data.<br>
f. Right to object - the right to object to our use of your Data including where we use it for our
legitimate interests.
			</li>
			<li>To make enquiries, exercise any of your rights set out above, or withdraw your consent to the
processing of your Data (where consent is our legal basis for processing your Data), please contact
us via this e-mail address: info@nutritionplanet.co.uk.
			</li>
			
			<li>If you are not satisfied with the way a complaint you make in relation to your Data is handled by
us, you may be able to refer your complaint to the relevant data protection authority. For the UK,
this is the Information Commissioner&#39;s Office (ICO). The ICO&#39;s contact details can be found on
their website at https://ico.org.uk/.
			</li>
			
			<li>It is important that the Data we hold about you is accurate and current. Please keep us informed if
your Data changes during the period for which we hold it.
			</li>
			
			
			
			<h4>Links to other websites</h4>
			<li>This Website may, from time to time, provide links to other websites. We have no control over
such websites and are not responsible for the content of these websites. This privacy policy does
not extend to your use of such websites. You are advised to read the privacy policy or statement of
other websites prior to using them.
			</li>
			
			<h4>Changes of business ownership and control</h4>
			
			<li>Curamed Limited may, from time to time, expand or reduce our business and this may involve the
sale and/or the transfer of control of all or part of Curamed Limited. Data provided by Users will,
where it is relevant to any part of our business so transferred, be transferred along with that part
and the new owner or newly controlling party will, under the terms of this privacy policy, be
permitted to use the Data for the purposes for which it was originally supplied to us.
			</li>
			<li>We may also disclose Data to a prospective purchaser of our business or any part of it.
			</li>
			<li>In the above instances, we will take steps with the aim of ensuring your privacy is protected.
			</li>
			
			<h4>General</h4>
			<li>You may not transfer any of your rights under this privacy policy to any other person. We may
transfer our rights under this privacy policy where we reasonably believe your rights will not be
affected.
			</li>
			<li>If any court or competent authority finds that any provision of this privacy policy (or part of any
provision) is invalid, illegal or unenforceable, that provision or part-provision will, to the extent
required, be deemed to be deleted, and the validity and enforceability of the other provisions of
this privacy policy will not be affected.
			</li>
			<li>Unless otherwise agreed, no delay, act or omission by a party in exercising any right or remedy
will be deemed a waiver of that, or any other, right or remedy.
			</li>
			<li>This Agreement will be governed by and interpreted according to the law of England and Wales.
All disputes arising under the Agreement will be subject to the exclusive jurisdiction of the
English and Welsh courts.
			</li>
			
			<h4>Changes to this privacy policy</h4>
			
			<li>Curamed Limited reserves the right to change this privacy policy as we may deem necessary from
time to time or as may be required by law. Any changes will be immediately posted on the Website
and you are deemed to have accepted the terms of the privacy policy on your first use of the
Website following the alterations.
			</li>
			
			You may contact Curamed Limited by email at info@nutritionplanet.co.uk.<br>
01 July 2019
			
			
			</ol>
		</li>
		
	</ol>

</div>
@endsection