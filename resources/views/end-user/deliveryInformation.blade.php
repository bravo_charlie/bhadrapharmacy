@extends('layout')
@section('content')
<div class="container our-guarantee  animated fadeInUp animatedfadeInUp">
	
	

		
			
				<h5 class="mb-0">
						Delivery Information
				</h5>
		

				
					<h4 style="margin-top:40px;">UK Delivery</h4>

					<p>
						<table class="table table-hover">
  <thead>
    <tr>
      <th scope="col">Delivery Option</th>
      <th scope="col">Price (£)</th>
      </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">Standard Delivery<br><p>Orders are delivered within 2-4 working days</P></th>
      
      <td>£3.00</td>
    </tr>
    <tr>
      <th scope="row">Premium Delivery<br><p>orders are delivered within 1-2 working days</P></th>
      
      <td>£5.50</td>
    </tr>
   
  </tbody>
</table>

<h6>Delivery Time</h6>
<p>Nutrition Planet does not take any responsibility for any other loss an order undelivered on
time may cause.</p>
					</P>
			
			
		
		
		
		
	
</div>
@endsection