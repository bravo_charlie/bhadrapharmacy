@extends('layout')

@section('content')
<section class="homepage-slider">
	<div class="container">
		
		<div class="row">
			<div class="col-sm-12 animated fadeInUp animatedfadeInUp">
				<p class="text-center welcome-asia">{{$cat_name}}</p>
							</div>

		</div>

		<div class="row">
			<div class="col-sm-2">
				<div class="filter-block p-1">
					<h4>Price</h4>
					<ul>
						@foreach($priceFilterArray as $pFilter)
						<li>
							<input class="price-filter" type="checkbox" name="filter-price" value="{{ $pFilter['url'] }}"
							{{ ($pFilter['slug'] == $priceFilter)?'checked':'' }} >
							<label>{{ $pFilter['title'] }}</label>
						</li>
						@endforeach
					</ul>
				</div>
			</div>
			<div class="col-sm-10">
				<div class="menu-product animated fadeInUp animatedfadeInUp">
					<div class="row">
						<div class="col-sm-12">
							<div class="pb-3">
								<h5 class="d-inline">Sort By:</h4>
									<select name="sort" class="sort-select">
										<option value="">Select Order By</option>
										@foreach($sortArray as $sort)
										<option value="{{$sort['url']}}" {{ ($sort['slug']==$order)?'selected':'' }}>{{ $sort['title'] }}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							@foreach($product_info as $info)
							<div class="col-sm-3 zoom-effect">
								<header>
									<a href="/product-detail/{{$info->id}}"><img src="/{{$info->featured_image1}}" class="cat-img"/></a>
								</header>
								<div class="main-product">
									<a href="/product-detail/{{$info->id}}">
										<h4 class="cat-name">
											{{$info->name}}
											<p>
												@if(empty($info->discount))
												£ {{ $info->price }}
												@else
												£ {{ $info->price-$info->discount }} <strike class="price-strike">£ {{ $info->price }}</strike>
												@endif
											</p>
										</h4>
									</a>
								</div>
							</div>
							@endforeach
						</div>
						{{ $product_info->appends(request()->input())->links() }}
					</div>
				</div>
			</div>

		</div>
	</section>

	
	@endsection

	@section('extra-js')
	<script type="text/javascript">
		$(document).ready(function() {
			$('.price-filter').click(function() {
				var url = window.location.href.split('?')[0];
				if(this.checked) {
					location.href = url+this.value;
				} else {
					location.href = url;
				}

			});

			$('.sort-select').change(function() {
				$this = $(this);
				var url = window.location.href.split('?')[0];
				location.href = url+$this.val();
			});
		});
	</script>
	
	@endsection