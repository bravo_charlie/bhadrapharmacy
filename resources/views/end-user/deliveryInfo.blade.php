@extends('layout')
@section('content')
<div class="container our-guarantee  animated fadeInUp animatedfadeInUp">
	
	
	<div class="accordion" id="faq_help">
		
			
				<h4>
					
						Product Returns
					
				</h4>
			

				
				<p>	Non-defective products can be returned within 14 Days of invoice, in new condition with all
original packing material and invoice/receipts for a refund minus the original shipping cost
we paid to ship the product to you. The shipping charge will not be applicable in the event an
incorrect item was shipped. All items purchased from Nutrition Planet are made pursuant to a
shipment contract. This basically means that the risk of loss and title for such items pass to
you upon our delivery to the carrier.
For health and safety reasons, we will not be able to accept returns or refunds on any foods
items that have been used, opened or had their security seal broken
</p>
				
		
		
		
		
			
				<h4>
						Return Policy
				</h4>
			
				
				<p>	You the customer must contact us at Nutrition Planet within 5 working days if your parcel
has not been received within the allocated shipping time for your destination. If you do not
and the parcel is returned to us you will be required to pay for a second shipping charge at
full cost. If for any reason at all you simply change your mind and wish to have you order
refunded you will be charged the cost of shipping if we have already sent it out you. Should
you wish to make a claim relating to your product i.e. damaged in transport, incorrect goods
sent etc. This must be done within 24 hours of receiving your goods by email. After
confirming any claim we shall promptly re-supply the goods or credit your account in full.
Any cost for the re-shipping of product shall be the responsibility of Nutrition Planet no
additional charges shall be the responsibility of our customers.
				</p>
			
		
		
			
				<h4>
						Product Exchange
				</h4>
		
			
			<p>
					We will gladly exchange our products, providing they return to our store within 14 Days of
invoice and are in the same condition which they left. The costs for the shipping to return the
parcel to Nutrition Planet, will be covered by the customer and the costs for the replacement
to be sent out would also need to be covered by the customer.
</p>
				
			
		
	
</div>
</div>
@endsection
