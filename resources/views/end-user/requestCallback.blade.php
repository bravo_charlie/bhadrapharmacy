@extends('layout')
@section('content')
<div class="container our-guarantee  animated fadeInUp animatedfadeInUp">
	<h2>Call Back Request</h2>
	<p class="topic">Would you like a call back from a bodykind Customer Service Representative?</p>
	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
	</p>
	<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
	<hr class="separator"/>
	<div class="callbackrequestform">
		<form action="/requestcallback" method="post">
			@csrf
			<div class="form-group">
				<label for="name">Name</label>
				<input type="text" class="form-control" required name="name"/>
			</div>
			<div class="form-group">
				<label for="telephone">Telephone</label>
				<input type="text" class="form-control" required name="telephone"/>
			</div>
			<div class="form-group">
				<label for="email">Email</label>
				<input type="text" class="form-control" required name="email"/>
			</div>
			<div class="form-group">
				<label for="time">Best Time to Call</label>
				<input type="text" class="form-control" required name="time"/>
			</div>
			<div class="form-group">
				<textarea class="form-control" name="comments" rows="4">Comments</textarea>
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-primary">Submit</button>
			</div>
		</form>
	</div>
</div>
@endsection