@extends('layout')
@section('content')
<div class="container termsof-use  animated fadeInUp animatedfadeInUp">
	<h2>Cookies policy</h2>
		<div class="termsofuse-content">
		<ol>
			<li><p>This Website may place and access certain Cookies on your computer. Curamed Limited uses
Cookies to improve your experience of using the Website and to improve our range of products.
Curamed Limited has carefully chosen these Cookies and has taken steps to ensure that your
privacy is protected and respected at all times.</p></li>
			<li><p>TAll Cookies used by this Website are used in accordance with current UK and EU Cookie Law.</p></li>
			<li><p>Before the Website places Cookies on your computer, you will be presented with a message bar
requesting your consent to set those Cookies. By giving your consent to the placing of Cookies,
you are enabling Curamed Limited to provide a better experience and service to you. You may, if
you wish, deny consent to the placing of Cookies; however certain features of the Website may not
function fully or as intended.</p></li>
<li><p>This Website may place the following Cookies:</li>
<table class="table table-bordered">
<thead>
<tr>
<th> Type of Cookie Purpose</th>
<th>Purpose</th>
</tr>
</thead>
<tbody>
<tr>
<td>Strictly necessary cookies</td>
<td>These are cookies that are required for the
operation of our website. They include, for
example, cookies that enable you to log into
secure areas of our website, use a shopping cart
or make use of e-billing services.</td>
</tr>

<tr>
<td>Analytical/performance cookies</td>
<td>They allow us to recognise and count the
number of visitors and to see how visitors
move around our website when they are using
it. This helps us to improve the way our
website works, for example, by ensuring that
users are finding what they are looking for
easily.</td>
</tr>

<tr>
<td>Functionality cookies</td>
<td>These are used to recognise you when you
return to our website. This enables us to
personalise our content for you, greet you by
name and remember your preferences (for
example, your choice of language or region).</td>
</tr>


</tbody>
</table>


<li><p>You can find a list of Cookies that we use in the Cookies Schedule.</p></li>
<li><p>You can choose to enable or disable Cookies in your internet browser. By default, most internet
browsers accept Cookies but this can be changed. For further details, please consult the help menu
in your internet browser.</p></li>
<li><p>You can choose to delete Cookies at any time; however you may lose any information that enables
you to access the Website more quickly and efficiently including, but not limited to,
personalisation settings.</p></li>
<li><p>It is recommended that you ensure that your internet browser is up-to-date and that you consult the
help and guidance provided by the developer of your internet browser if you are unsure about
adjusting your privacy settings.</p></li>
<li><p>For more information generally on cookies, including how to disable them, please refer to
aboutcookies.org. You will also find details on how to delete cookies from your computer.</p></li>
		</ol>	
	</div>
</div>
@endsection