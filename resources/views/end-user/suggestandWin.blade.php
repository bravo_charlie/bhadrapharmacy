@extends('layout')

@section('content')
<div class="container animated fadeInUp animatedfadeInUp">
	@if ($errors->any())
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
	@endif
	<h2>Suggest &amp; Win</h2>
	<h5>Send us your suggestion and win £50.00 to spend at bodykind!</h5>
	<hr class="separator"/>
	<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</p>
	<p>Simply complete the form below, click submit and thats it! We'll add your suggestion & details to our competition and you could be the winner!</p>
	<div class="suggestandwinform">
		<form method="post" action="/suggest-and-win">
			@csrf
			<div class="form-group">
				<label for="suggestion">Your Suggestion</label>
				<input type="text" name="suggestion" class="form-control" required/>
			</div>
			<div class="form-group">
				<label for="title">Title</label>
				<select name="title" class="form-control">
					<option value="Mr.">Mr.</option>
					<option value="Mrs.">Mrs.</option>
					<option value="Ms.">Ms.</option>
					<option value="Miss">Miss</option>
					<option value="Dr.">Dr.</option>
					<option value="Er.">Er.</option>
					<option value="Prof.">Prof.</option>
				</select>
			</div>
			<div class="form-group">
				<label for="firstname">First Name</label>
				<input type="text" name="firstname" class="form-control" required/>
			</div>
			<div class="form-group">
				<label for="lastname">Last Name</label>
				<input type="text" name="lastname" class="form-control" required/>
			</div>
			<div class="form-group">
				<label for="email">Email</label>
				<input type="text" id="email" name="email" class="form-control" required/>
			</div>
			<div class="form-group">
				<label for="confirm-email">Confirm Email</label>
				<input type="text" id="confirm-email" name="confirm-email" class="form-control" required/>
			</div>
			<div class="form-group">
				<button type="submit" class="form-control btn-primary">Submit</button>
			</div>
		</form>
	</div>
</div>
@endsection