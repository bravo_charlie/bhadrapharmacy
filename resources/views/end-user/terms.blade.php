@extends('layout')

@section('content')
<section class="homepage-slider">
	<div class="container animated fadeInUp animatedfadeInUp">
		
		<div class="row">

			<div class="col-sm-12"><h1 class="bold">Terms & Conditions</h1></div>
			<div class="col-sm-12">
				{!! !empty($data)?$data->contents:'' !!}
			</div>
		</div>

	</div>

</div>
</section>

@endsection