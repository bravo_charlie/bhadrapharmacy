@section('extra-css')
  <style>
  {
    margin: 4px;
    font-size: 16px;
    font-weight: bolder;
    cursor: pointer;
  }
  </style>
  @endsection
  @section('extra-js')
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
 @endsection
<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="/" class="brand-link">
    <img src="/images/homepage-images/nutritionplanet.png" style="width: 100%;">
    <span class="brand-text font-weight-light"></span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
     <!--  <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
         <a class="navbar-brand" href="/"><img src="/images/homepage-images/nutritionplanet.jpg" style="width: 100%;"></a>
        </div>
        <div class="info">
          <a href="#" class="d-block">Alexander Pierce</a>
        </div>
      </div> -->

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false" >
          <!-- Add icons to the links using the .nav-icon class
          with font-awesome or any other icon font library -->

        <li class="nav-item">
             @role('super-admin')
          <li class="nav-item has-treeview menu-open">
           <a href="{{route('home')}}" class="list-group-item " >

           Admin Dashboard

          </a>
        </li>
          <li>
            <a href="/slider" class="list-group-item " >Slider Creation
              <span class="caret"></span>
            </a>
          </li>

            <li>
              <a href="/product-category" class="list-group-item " >Add Category
                <span class="caret"></span>
              </a>
            </li>
             <li>
              <a href="/product-category/show" class="list-group-item  " > Category List
                <span class="caret"></span>
              </a>
            </li>
            <li>
              <a href="/products-show" class="list-group-item " >Products List
               <span class="caret"></span>
             </a>
            </li>
            <li>
              <a href="/delivery" class="list-group-item" >Delivery Prices List
                <span class="caret"></span>
              </a>
            </li>
            <li>
             <a href="/product" class="list-group-item " >Product<span class="caret"></span> </a>
           </li>
            <li>
             <a href="/menu" class="list-group-item " >Menu Creation<span class="caret"></span></a>
           </li>
            <li>
             <a href="/subMenu" class="list-group-item " >Submenu Creation<span class="caret"></span></a>
           </li>
           <li>
              <a href="/coupon" class="list-group-item" >Coupons
                <span class="caret"></span>
              </a>
            </li>
           <li>
              <a href="/order-history" class="list-group-item " >Orders<span class="caret"></span></a>
            </li>
            <li>
              <a href="/terms-create-edit" class="list-group-item " >Terms & Conditions<span class="caret"></span></a>
            </li>
            <li>
              <a href="/privacy-create-edit" class="list-group-item " >Privacy<span class="caret"></span></a>
            </li>
            <li>
              <a href="/users" class="list-group-item " >User Management<span class="caret"></span></a>
            </li>

          @else
          <li class="nav-item has-treeview menu-open">
           <a href="{{route('home')}}" class="list-group-item " >

           Customer Dashboard

          </a>
        </li>

               <div id="main-menu" class="list-group">

                   <li>
                    <a href="/profile" class="list-group-item " >Profile<span class="caret"></span></a>
                  </li>

                   <li>
                    <a href="/order-history" class="list-group-item " >Order History<span class="caret"></span></a>
                  </li>
                   <li>
                    <a href="/checkout" class="list-group-item " >Checkout<span class="caret"></span></a>
                  </li>
              </div>

         @endrole
       </li>



</ul>

</nav>

</div>


</aside>
@section('extra-js')
<script>
  jQuery(function($) {
     var path = window.location.href;
     $('li a').each(function() {
      if (this.href === path) {
       $(this).addClass('active');
      }
     });
    });
</script>
@endsection
