<div class="footer" >
  <div class="container">
    <div class="row">
      <div class="col-sm-4">
          <h5>
            About 
          </h5>
          <ul>
            <li><a href="/about-us">About Us</a></li>
            <li><a href="/privacy-policy">Privacy Policy</a></li>
            <li><a href="/terms-of-use">Cookies Policy</a></li>
            <li><a href="/terms-and-condition">Terms & Conditions</a></li>
          </ul>
      </div>
      <div class="col-sm-4">
          <h5>
            Customer Services
          </h5>
          <ul>
            <li><a href="/contact-us">Contact Us</a></li>
            <li><a href="/help-faq">Delivery Information</a></li>
            <li><a href="/return-policy">Return Policy</a></li>
                      </ul>
      </div>
      <div class="col-sm-4">
          <h5>
            Community
          </h5>
          <ul>
            <li><a href="/home">My Account</a></li>
                      </ul>
      </div>
      
    </div>
  </div>
</div>
<div class="developer-designing">
  <div class="container">
    <div class="row">
      <div class="col-sm-7">
        <p>Copyright © Curamed Limited, 2019. All rights reserved. nutritionplanet.co.uk is a trading
name of Curamed Limited. Registered office: | Devonshire House
582 Honeypot Lane
Stanmore
Middlesex
HA7 1JS</p>
      </div>
     
      <div class="col-sm-5">
        <p>Registered in England: company no. 05142946. Registered VAT no. 132732143.</p>
      </div>
    </div>
  </div>
</div>
</div> 
<div id="mhra-fmd-placeholder" >
<div class="marph-logo" style="margin:0px auto; padding:20px; text-align:center;">
<a href="http://medicine-seller-register.mhra.gov.uk/search-registry/514" target="_blank">
<img src="https://pclportal.mhra.gov.uk/analytics/logo-preview-small.png" />
</a>
</div>
</div>