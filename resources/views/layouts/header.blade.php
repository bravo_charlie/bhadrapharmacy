
 
     <div class="top-header" id="topHeader">

      <div class="container-fluid">
        <div class='row'>
           <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
          <div class='col-sm-3 header'>
        
         <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="/" class="nav-link">Home</a>
      </li>
      
    </ul>
  </div>
     <div class="col-sm-6 header">
            <form action="/queries/search" method="POST" role="search" id="search-bar">
              {{ csrf_field() }}
              <div class="input-group">
                <input type="text" class="form-control" name="search" placeholder="Search for a product"> 
                <span class="input-group-btn">
                  <button type="submit" class="btn btn-default">
                    <i class="fa fa-search"></i>
                  </button>
                </span>
              </div>
            </form>
          </div>
          <div class='col-sm-3 header'>
                  <div class="pull-right">
                    <span class="cart-block-admin"><cart-list></cart-list></span>
                    <a href="{{ route('logout') }}" class="logoutheader" style="color:white; font-family: 'Arial, Helvetica, sans-serif';font-size:1.3rem">
                           <i class="fa fa-sign-out" style="color:white"></i><span class='logout'>logout</span>
                    </a>
                </div>
            
         
          </div>
           </nav>
    </div>
       

         
        
      </div>
     
    </div>
 
