<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" xmlns:v-on="http://www.w3.org/1999/xhtml">
<head>
    <title>@yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="{{ url('img/fav.ico') }}" type="image/x-icon">
</head>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<link rel="stylesheet" type="text/css" href="/css/menu.css" />

<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700" rel="stylesheet">
<script stc="https://unpkg.com/vue@2.1.3/dist/vue.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>


<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript">

    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>


<style type="text/css">
    .dropdown-item{
        text-align: left !important;
        padding: 0.25rem 1.5rem !important;
        text-align: inherit !important;
    }
    .dropdown-menu.show {
        margin-left: 13px;
        margin-top: -6px;
    }
    .table-vin-number {
        line-height: .8;
        color: #11a2cf !important;
    }
    .profile{
        padding:13px !important;
    }
    a:hover {
        color: #11a2cf;
        text-decoration: none !important;
    }
    body, input {
        font-family: 'Roboto', sans-serif !important;
        font-weight: 400 !important;
        font-size: 16px !important;
    }
    .pointer-class{
        cursor: pointer;
    }
    button.light_gray{
        cursor: pointer;
        height: 50px;
        border-radius: 30px;
        border: none;
        width: 100%;
        font-size: 18px;
        font-weight: 400;
        box-shadow: none !important;

    }

    .clear-search{
        box-shadow: none !important;
    }
    .plr{
        padding-left: 16px;
        padding-right: 17px;
    }
    .plr0{
        padding-left: 0px !important;
        padding-right: 0px !important;
    }
    .pl30{
        padding-left: 30px !important;
    }
</style>
@stack('mystyles')
<body>


<section class="top-nav">
    <div class="container-wrapper container-fluid">
        <div class="row">
            <div class="col-sm-1 text-left">
                <li><a href="/" class="company-logo"><img src="{{ url('images/icons/chemist.png') }}" width="170px"></a></li>
            </div>
            <div class="col-sm-9">
                @include('partial.navbar')
            </div>
            <div class="col-sm-2 text-right">
  
                <ul style="margin-top: 13px;">
                    <li class="dropdown">
                    
                        <ul class="dropdown-menu">
                            <div class="top_user_menu">
                             
                                        <li>
                                            <a class="dropdown-item myDealers" id="myDealers" dealer="">
                                                  Chemist
                                            </a>
                                        </li>
                                 
                            </div>
                                <div class="bottom_user_menu">
                            <li>
                                    <a class="dropdown-item" href="{{ url('/setting/dealer-profile') }}">Settings</a>
                                    <a class="dropdown-item" href="">Support</a>
                                    <a class="dropdown-item" href="{{ route('logout') }}">Log Out</a>
                            </li>
                                </div>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<div class="container" style="margin-top:20px;">
    <div class="row">
        <div class="col-sm-12">
            @include('layouts.success_message')
        </div>
    </div>
</div>
<div id="app">
@yield('content')
</div>


<!-- Modal -->
<div class="modal fade" id="pageNotFoundModal" tabindex="-1" role="dialog" aria-labelledby="pageNotFoundModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content my-modal">
            <div class="modal-body text-center">
                <div class="row">
                    <div class="col-md-12 pt-10">
                        <img src="/img/danger.svg" height=88>
                    </div>
                    <div class="col-md-12 pt-50">
                        <p>Sorry, it looks like you do not have permission
                        to view this page.</p>
                    </div>
                    <div class="col-md-12 pt-40">
                        <button type="button" class="common_btn white_text light-btn" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
         
        </div>
    </div>
</div>
</body>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.2/umd/popper.min.js"></script>
@stack('myScripts')
</html>