@extends('layouts.admin')

@section('content')
<div class="container-fluid">
@if($errors->any())
	<div class="alert alert-danger">
		<ul>
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul><br/>
	</div>
@endif
<div class="row">
	
		<div class="col-sm-9 admin_playzone">
			<h3>Submenu</h3>
	<a href="{{url('/subMenu/create')}}" class="btn btn-primary">Create Submenu</a>
	<table class="menu_layout table table-hover">
		<thead>
			<th>S.N</th>
			<th>Main Menu</th>
			<th>Sub Menu</th>
			<th>Order</th>
			<th>Action</th>
		</thead>
		
		@foreach($sub_Menu as $submenu)
		<tbody>
			<td>{{ $submenu->id}}</td>
			<td>{{ $submenu->menu->menuName }}</td>
			<td>{{ $submenu->subMenuName }}</td>
			<td>{{ $submenu->order }}</td>
			<td>
				<a class="btn btn-sm btn-info" href="/subMenu/edit/{{$submenu->id}}">Edit</a><br>
				<a class="btn btn-sm btn-danger" href="/subMenu/delete/{{$submenu->id}}" onclick="deleteAction(event)">Delete</a>
			</td>
		</tbody>
		@endforeach
		
	</table>
		</div>
</div>
</div>
@endsection