
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('users-list', require('./components/UsersList.vue').default);
Vue.component('cart-list', require('./components/CartList.vue').default);
Vue.component('cart-update', require('./components/CartUpdate.vue').default);
Vue.component('delivery', require('./components/Delivery.vue').default);
Vue.component('delivery-options', require('./components/DeliveryOptions.vue').default);
Vue.component('delivery-details', require('./components/DeliveryDetails.vue').default);
Vue.component('order-history', require('./components/Orders/OrderHistory.vue').default);
Vue.component('orders', require('./components/Admin/Orders.vue').default);
Vue.component('product-categories-select', require('./components/ProductCategoriesSelect.vue').default);
Vue.component('proceed-checkout-button', require('./components/ProceedCheckoutButton.vue').default);
Vue.component('coupons', require('./components/Coupons/Coupons.vue').default);
Vue.component('apply-coupon', require('./components/Coupons/ApplyCoupon.vue').default);
Vue.component('text-editor', require('./components/TextEditor.vue').default);
Vue.component('checkout', require('./components/Checkout/Checkout.vue').default);

const app = new Vue({
    el: '#app',
    mounted() {
        let timezoneOffset = new Date().getTimezoneOffset()
        // Set the timezone cookie so the dates can
        // be displayed in user's local time
        this.$cookie.set('tz', timezoneOffset)
    }
});